import { Grid } from "@mui/material";
import LocationOnIcon from '@mui/icons-material/LocationOn';
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';

const Question = () => {
    return (
        <Grid className="slide-link" item xs={12} md={3}>
            <div className="footer-text">
                <h2 className="footer-header">Hỗ trợ khách hàng</h2>
                <p><LocationOnIcon fontSize="small" /> &nbsp; 282/59 Bùi Hữu Nghĩa, Phường 2, Quận Bình Thạnh, TP.HCM</p>
                <p><a href="#shop"><PhoneIcon fontSize="small" /> &nbsp; +84 916 949 139</a></p>
                <p><a href="#shop"><EmailIcon fontSize="small" /> &nbsp; trieu.nc1993@gmail.com</a></p>
            </div>
        </Grid>
    )
}

export default Question;