import { Grid } from "@mui/material";
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import InstagramIcon from '@mui/icons-material/Instagram';

const Information = () => {
    return (
        <Grid item xs={12} md={3}>
            <div className="footer-text">
                <h2 className="footer-header">2T Smart Shop</h2>
                <p>
                    Shop chuyên đồng hồ thông minh đầu tiên tại Việt Nam.
                    Tuổi đời hoạt động 5 năm với nhiều kinh nghiệm tư vấn và chia sẻ khách hàng.
                </p>
            </div>
            <Grid container mt={3}>
                <a href="/twitter" className="social-icon"><TwitterIcon /></a> &nbsp; &nbsp;
                <a href="/facebook" className="social-icon"><FacebookRoundedIcon /></a> &nbsp; &nbsp;
                <a href="/instagram" className="social-icon"><InstagramIcon /></a>
            </Grid>
        </Grid>
    )
}

export default Information;