import { Grid } from "@mui/material";

const Menu = () => {
    return (
        <Grid className="slide-link" item xs={12} md={3} sx={ { pl: { xs: 0, md: 6 } } } >
            <div className="footer-text">
                <h2 className="footer-header">Menu</h2>
                <p><a href="#shop">Shop</a></p>
                <p><a href="#shop">Về Chúng Tôi</a></p>
                <p><a href="#shop">Blog</a></p>
                <p><a href="#shop">Liên Hệ</a></p>
            </div>
        </Grid>
    )
}

export default Menu;