import { Box, Button, Grid, Modal, Paper, TextField, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { adminUpdateCustomerAction, closeCustomerDetailModalAction, customerDetailFullnameChangeAction, customerDetailPhoneChangeAction } from "../../../actions/user.action";
import createAxiosInterceptorJWT from "../../../createAxiosJWT";
import CustomerDetailAddress from "./CustomerDetailAddress";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    p: 3,
    borderRadius: 2
};

const CustomerDetailModal = () => {
    const { userData, customerDetailModalOpen, customerDetail } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);

    // button update customer handler
    const updateCustomerHandler = () => {
        const customerId = customerDetail._id;
        dispatch(adminUpdateCustomerAction(userData, customerId, axiosJWT));
    }

    return (
        <>
            {
                customerDetail ? <Paper elevation={2}>
                    <Modal open={customerDetailModalOpen} onClose={() => dispatch(closeCustomerDetailModalAction())}>
                        <Box sx={style}>
                            <Typography textAlign="center" variant="h5" fontWeight={600}>THÔNG TIN KHÁCH HÀNG</Typography>
                            <Grid container mt={0} spacing={2} alignItems="center">
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        value={customerDetail.fullName}
                                        onChange={e => dispatch(customerDetailFullnameChangeAction(e.target.value))}
                                        label="Họ Và Tên" fullWidth size="small" margin="dense"
                                    />
                                    <Grid container spacing={1}>
                                        <Grid item xs={8}>
                                            <TextField
                                                value={customerDetail.user?.username} 
                                                label="Username" fullWidth size="small" margin="dense"
                                            />
                                        </Grid>
                                        <Grid item xs={4}>
                                            <TextField
                                                value={customerDetail.user?.role} 
                                                label="Hạng" fullWidth size="small" margin="dense"
                                            />
                                        </Grid>
                                    </Grid>
                                    <TextField
                                        value={customerDetail.email} 
                                        label="Email" fullWidth size="small" margin="dense"
                                    />
                                    <TextField
                                        value={customerDetail.phone}
                                        onChange={e => dispatch(customerDetailPhoneChangeAction(e.target.value))}
                                        label="Điện Thoại" fullWidth size="small" margin="dense" />
                                </Grid>
                                <Grid item xs={12} sm={6} textAlign="center">
                                    <img src={customerDetail.profilePic} style={{ borderRadius: 5, width: "180px", border: "1px solid gray" }} alt="avatar" />
                                </Grid>
                            </Grid>

                            <CustomerDetailAddress />

                            <Grid container mt={2} spacing={1} justifyContent="end">
                                <Grid item>
                                    <Button variant="contained" color="inherit" onClick={() => dispatch(closeCustomerDetailModalAction())}>Quay lại</Button>
                                </Grid>
                                <Grid item>
                                    <Button variant="contained" color="success" onClick={updateCustomerHandler}>Cập Nhật</Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Modal>
                </Paper> : null
            }

        </>
    )
}

export default CustomerDetailModal;