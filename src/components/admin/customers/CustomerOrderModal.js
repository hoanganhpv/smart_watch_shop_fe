import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Grid, Modal, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { customerOrderModalCloseAction } from "../../../actions/user.action";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1100,
    bgcolor: 'background.paper',
    p: 3,
    borderRadius: 2
};

const getStatusColor = (paramStatus) => {
    if (paramStatus === "Confirmed") {
        return "blue";
    }
    else if (paramStatus === "Canceled") {
        return "red";
    }
    else if (paramStatus === "Delivered") {
        return "green";
    }
    return "maroon";
}

const CustomerOrderModal = () => {
    const { customerOrderModalOpen, customerOrderDetail } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    // handle close modal
    const closeDeleteOrderModalHandler = () => {
        dispatch(customerOrderModalCloseAction());
    }

    return (
        <Modal open={customerOrderModalOpen} onClose={closeDeleteOrderModalHandler} >
            <Box sx={style}>
                <Typography textAlign="center" variant="h5" fontWeight={600}>THÔNG TIN ĐƠN HÀNG</Typography>
                <Grid container mt={4}>
                    <Grid item xs={12}>
                        {
                            customerOrderDetail.length > 0 ? customerOrderDetail.map((element, index) =>
                                    <Accordion sx={{ p: 1 }} key={element.orderCode} elevation={3}>
                                        <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                                            <Grid container spacing={1}>
                                                <Grid item xs={2} textAlign="center">
                                                    <Typography fontSize={15}><b>{element.orderCode}</b></Typography>
                                                </Grid>
                                                <Grid item xs={2} textAlign="right">
                                                    <Typography fontSize={15}><b>Ngày đặt hàng:</b></Typography>
                                                </Grid>
                                                <Grid item xs={2} textAlign="left">
                                                    <Typography fontSize={15}>{element.orderDate}</Typography>
                                                </Grid>
                                                <Grid item xs={2} textAlign="right">
                                                    <Typography fontSize={15}><b>Ngày giao hàng:</b></Typography>
                                                </Grid>
                                                <Grid item xs={2} textAlign="left">
                                                    <Typography fontSize={15}>{element.shippedDate || "Đơn chưa hoàn thành"}</Typography>
                                                </Grid>
                                                <Grid item xs={2} textAlign="center">
                                                    <Typography color={() => getStatusColor(element.status)}><b>{element.status}</b></Typography>
                                                </Grid>
                                            </Grid>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container>
                                                <Grid item xs={12}>
                                                    <Typography fontSize={16} fontWeight={500}>NGƯỜI NHẬN:</Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid container spacing={2}>
                                                <Grid item xs={3}>
                                                    <TextField value={element.fullName} size="small" fullWidth label="Họ và tên" margin="dense" />
                                                    <TextField value={element.email} size="small" fullWidth label="Email" margin="dense" />
                                                    <TextField value={element.phone} size="small" fullWidth label="Số điện thoại" margin="dense" />
                                                </Grid>
                                                <Grid item xs={5}>
                                                    <TextField value={element.note} size="small" fullWidth label="Ghi chú" margin="dense" />
                                                    <TextField
                                                        value={element.address + ", " + element.ward + ", " + element.district + ", " + element.city}
                                                        label="Địa chỉ" multiline minRows={3} fullWidth size="small" margin="dense"
                                                    />
                                                </Grid>
                                            </Grid>
                                            <Grid container>
                                                <Grid item xs={12}>
                                                    <Typography fontSize={16} fontWeight={500} mt={2}>CHI TIẾT:</Typography>
                                                </Grid>
                                            </Grid>
                                            <TableContainer sx={{ mt: 1 }} component={Paper} elevation={3}>
                                                <Table size="small" aria-label="customized table">
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell><b>Tên Sản Phẩm</b></TableCell>
                                                            <TableCell align="center"><b>Số Lượng</b></TableCell>
                                                            <TableCell align="right"><b>Đơn Giá</b></TableCell>
                                                            <TableCell align="right"><b>Thành Tiền</b></TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {
                                                            element.orderDetails.map((detail, detailIndex) =>
                                                                <TableRow key={detailIndex}>
                                                                    <TableCell>{detail.name}</TableCell>
                                                                    <TableCell align="center">{detail.quantity}</TableCell>
                                                                    <TableCell align="right">{detail.price.toLocaleString()}đ</TableCell>
                                                                    <TableCell align="right">{(detail.quantity * detail.price).toLocaleString()}đ</TableCell>
                                                                </TableRow>
                                                            )
                                                        }
                                                        <TableRow>
                                                            <TableCell><b>TỔNG CỘNG</b></TableCell>
                                                            <TableCell></TableCell>
                                                            <TableCell></TableCell>
                                                            <TableCell align="right"><b>{element.totalPrice.toLocaleString()}đ</b></TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </AccordionDetails>
                                    </Accordion>
                            ) : <Typography fontWeight={600}>Chưa có đơn hàng</Typography>
                        }
                    </Grid>
                </Grid>
                <Grid container mt={2} justifyContent="center" >
                    <Grid item>
                        <Pagination count={5} size="small" />
                    </Grid>
                </Grid>
                <Grid container justifyContent="end" spacing={1}>
                    <Grid item>
                        <Button color="inherit" variant="contained" onClick={closeDeleteOrderModalHandler}>Quay lại</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default CustomerOrderModal;