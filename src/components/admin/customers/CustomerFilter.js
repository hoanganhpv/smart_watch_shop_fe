import { Button, Grid, Paper, TextField, Typography } from "@mui/material";
import FilterAltIcon from '@mui/icons-material/FilterAlt';

const CustomerFilter = () => {
    return (
        <Paper sx={{ p: 1, my: 2 }}>
            <Grid container spacing={3} alignItems="center" justifyContent="center">
                <Grid item>
                    <Typography variant="h6">NHẬP THÔNG TIN CẦN TÌM:</Typography>
                </Grid>
                <Grid item xs={2}>
                    <TextField label="Nội Dung" size="small" fullWidth />
                </Grid>
                <Grid item>
                    <Button color="inherit" variant="outlined" startIcon={<FilterAltIcon />}>Lọc Dữ Liệu</Button>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default CustomerFilter;