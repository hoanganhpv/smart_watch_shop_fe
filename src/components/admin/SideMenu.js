import { AppBar, Box, Button, CssBaseline, Divider, Drawer, Grid, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Toolbar, Typography } from "@mui/material";
import InventoryIcon from '@mui/icons-material/Inventory';
import HomeIcon from '@mui/icons-material/Home';
import LocalMallIcon from '@mui/icons-material/LocalMall';
import PersonIcon from '@mui/icons-material/Person';
import { useState } from "react";
import HomeAdminPage from "./home/HomeAdminPage";
import OrderAdminPage from "./orders/OrderAdminPage";
import CustomerAdminPage from "./customers/CustomerAdminPage";
import ProductAdminPage from "./products/ProductAdminPage";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const drawerWidth = 240;
const text = {
    fontSize: '16px',
    fontWeight: 400,
    color: 'black',
}

const SideMenu = () => {
    const { userData } = useSelector(reduxData => reduxData.userReducer);
    const [homeSelect, setHomeSelect] = useState(true);
    const [orderSelect, setOrderSelect] = useState(false);
    const [customerSelect, setCustomerSelect] = useState(false);
    const [productSelect, setProductSelect] = useState(false);

    const btnHomeClickHandler = () => {
        setHomeSelect(true);
        setOrderSelect(false);
        setCustomerSelect(false);
        setProductSelect(false);
    }
    const btnOrderClickHandler = () => {
        setHomeSelect(false);
        setOrderSelect(true);
        setCustomerSelect(false);
        setProductSelect(false);
    }
    const btnCustomerClickHandler = () => {
        setHomeSelect(false);
        setOrderSelect(false);
        setCustomerSelect(true);
        setProductSelect(false);
    }
    const btnProductClickHandler = () => {
        setHomeSelect(false);
        setOrderSelect(false);
        setCustomerSelect(false);
        setProductSelect(true);
    }

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                elevation={0}
                sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px`, backgroundColor: "#f0f0f0" }}
            >
                <Toolbar>
                    <Box className="zoom-link" sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, justifyContent: "flex-end", alignItems: "center" }}>
                        <Link to="/">
                            <Button
                                sx={{ color: 'black', display: 'block', fontSize: "12px", letterSpacing: "2px" }}
                            >
                                Trang Chủ 2T Smart Shop
                            </Button>
                        </Link>
                    </Box>
                </Toolbar>
            </AppBar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                        backgroundColor: "#f0f0f0",
                    },
                }}
                variant="permanent"
                anchor="left"
            >
                <Toolbar>
                    <Grid container >
                        <Typography color="black" variant="h6" noWrap component="div">
                            2T SMART SHOP
                        </Typography>
                    </Grid>
                </Toolbar>
                <Divider />
                <Toolbar>
                    <Grid container >
                        <Typography color="black" variant="h6" noWrap component="div">
                            {userData.fullName}
                        </Typography>
                    </Grid>
                </Toolbar>

                <Divider />
                <List >

                    <ListItem dense>
                        <ListItemButton  sx={{borderRadius: 3}} selected={homeSelect} onClick={btnHomeClickHandler}>
                            <ListItemIcon sx={{ color: "black" }}>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primaryTypographyProps={{ style: text }} primary="Trang Chủ" />
                        </ListItemButton>
                    </ListItem>

                    <ListItem dense>
                        <ListItemButton sx={{borderRadius: 3}} selected={orderSelect} onClick={btnOrderClickHandler}>
                            <ListItemIcon sx={{ color: "black" }}>
                                <LocalMallIcon />
                            </ListItemIcon>
                            <ListItemText primaryTypographyProps={{ style: text }} primary="Đơn Hàng" />
                        </ListItemButton>
                    </ListItem>

                    <ListItem dense>
                        <ListItemButton sx={{borderRadius: 3}} selected={customerSelect} onClick={btnCustomerClickHandler}>
                            <ListItemIcon sx={{ color: "black" }}>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText primaryTypographyProps={{ style: text }} primary="Khách Hàng" />
                        </ListItemButton>
                    </ListItem>

                    <ListItem dense>
                        <ListItemButton sx={{borderRadius: 3}} selected={productSelect} onClick={btnProductClickHandler}>
                            <ListItemIcon sx={{ color: "black" }}>
                                <InventoryIcon />
                            </ListItemIcon>
                            <ListItemText primaryTypographyProps={{ style: text }} primary="Sản Phẩm" />
                        </ListItemButton>
                    </ListItem>

                </List>
            </Drawer>
            <Box
                component="main"
                sx={{ flexGrow: 1, bgcolor: '#fafafa', p: 3 }}
            >
                <Toolbar />
                {homeSelect ? <HomeAdminPage /> : orderSelect ? <OrderAdminPage /> : customerSelect ? <CustomerAdminPage /> : <ProductAdminPage />}
            </Box>
        </Box>
    )
}

export default SideMenu;