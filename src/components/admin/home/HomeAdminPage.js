import { Box, Typography } from "@mui/material";

const HomeAdminPage = () => {
    return (
        <Box sx={{ minHeight: 845 }}>
            <Typography textAlign="center" variant="h4" fontWeight={600}>TRANG CHỦ</Typography>
        </Box>
    )
}

export default HomeAdminPage;