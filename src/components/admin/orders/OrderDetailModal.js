import { Box, Button, FormControl, Grid, InputLabel, MenuItem, Modal, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { orderDetailCloseAction, orderNoteChangeAction, orderStatusChangeAction, statusUpdateHandlerAction, updateOrderHandlerAction } from "../../../actions/user.action";
import CheckIcon from '@mui/icons-material/Check';
import createAxiosInterceptorJWT from "../../../createAxiosJWT";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    p: 3,
    borderRadius: 2
};

const OrderDetailModal = () => {
    const { orderDetailModalOpen, orderDetail, currentOrderStatus } = useSelector(reduxData => reduxData.userReducer);
    const [totalPayment, setTotalPayment] = useState(0);
    const dispatch = useDispatch();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);

    // calculate total payment:
    useEffect(() => {
        if (orderDetail) {
            let totalPrice = 0;
            orderDetail.orderDetails.forEach(element => totalPrice += (element.quantity * element.product.promotionPrice));
            setTotalPayment(totalPrice);
        }
    }, [orderDetail]);

    // order status update handler
    const statusUpdateHandler = (event) => {
        const value = event.target.value;
        dispatch(orderStatusChangeAction(value));
    }

    // order note change handler
    const noteChangeHandler = (event) => {
        const value = event.target.value;
        dispatch(orderNoteChangeAction(value));
    }

    // button confirm status click handler:
    const btnConfirmStatusChangeHandler = () => {
        const orderStatus = orderDetail.status;
        const orderId = orderDetail._id;
        dispatch(statusUpdateHandlerAction(orderStatus, orderId, axiosJWT));
    }

    // button update handler: 
    const btnUpdateOrderHandler = () => {
        const orderId = orderDetail._id;
        const orderNote = orderDetail.note;
        dispatch(updateOrderHandlerAction(orderNote, orderId, axiosJWT));
    }

    return (
        <>
            {
                orderDetail ? <Paper elevation={2}>
                    <Modal open={orderDetailModalOpen} onClose={() => dispatch(orderDetailCloseAction())}>
                        <Box sx={style}>
                            <Typography textAlign="center" variant="h5" fontWeight={600}>THÔNG TIN ĐƠN HÀNG</Typography>
                            <Grid container mt={0} spacing={2}>
                                <Grid item xs={12} sm={6}>
                                    <TextField value={orderDetail.orderCode} label="Mã Đơn Hàng" fullWidth size="small" margin="dense" />
                                    <TextField value={orderDetail.orderDate} label="Ngày Đặt Hàng" fullWidth size="small" margin="dense" />
                                    <TextField value={orderDetail.fullName} label="Họ Và Tên" fullWidth size="small" margin="dense" />
                                    <TextField value={orderDetail.email} label="Email" fullWidth size="small" margin="dense" />
                                    <TextField value={orderDetail.phone} label="Điện Thoại" fullWidth size="small" margin="dense" />

                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <Grid container spacing={1} alignItems="center">
                                        <Grid item xs={6}>
                                            <FormControl size="small" fullWidth margin="dense" disabled={currentOrderStatus === "Delivered" || currentOrderStatus === "Canceled" ? true : false} >
                                                <InputLabel>Trạng Thái</InputLabel>
                                                <Select
                                                    value={orderDetail.status}
                                                    label="Trạng Thái"
                                                    onChange={statusUpdateHandler}
                                                >
                                                    <MenuItem value="New">New</MenuItem>
                                                    <MenuItem value="Canceled">Canceled</MenuItem>
                                                    <MenuItem value="Confirmed">Confirmed</MenuItem>
                                                    <MenuItem value="Delivered">Delivered</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Button 
                                                variant="outlined" color="success" fullWidth sx={{mt: "4px"}} startIcon={<CheckIcon sx={{mb: "5px"}} />}
                                                onClick={btnConfirmStatusChangeHandler} 
                                                disabled={currentOrderStatus === "Delivered" || currentOrderStatus === "Canceled" ? true : false}>
                                                    Xác nhận
                                            </Button>
                                        </Grid>
                                    </Grid>

                                    <TextField value={orderDetail.shippedDate} label="Ngày Giao Hàng" fullWidth size="small" margin="dense" />
                                    <TextField
                                        value={`${orderDetail.address}, ${orderDetail.ward}, ${orderDetail.district}, ${orderDetail.city}`}
                                        label="Địa Chỉ" multiline minRows={2} fullWidth size="small" margin="dense"
                                    />
                                    <TextField
                                        sx={{mt: "14px"}}
                                        value={orderDetail.note}
                                        onChange={noteChangeHandler}
                                        label="Ghi Chú" multiline minRows={2} fullWidth size="small" margin="dense"
                                    />
                                </Grid>
                            </Grid>
                            <Typography mt={1} variant="h6" fontWeight={500}>CHI TIẾT:</Typography>

                            <TableContainer sx={{ mt: 1 }} component={Paper} elevation={1}>
                                <Table size="small" aria-label="customized table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Tên Sản Phẩm</TableCell>
                                            <TableCell align="center">Số Lượng</TableCell>
                                            <TableCell align="right">Đơn Giá</TableCell>
                                            <TableCell align="right">Thành Tiền</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            orderDetail ? orderDetail.orderDetails.map((element, index) => {
                                                return (
                                                    <TableRow key={index}>
                                                        <TableCell>{element.product.name}</TableCell>
                                                        <TableCell align="center">{element.quantity}</TableCell>
                                                        <TableCell align="right">{element.product.promotionPrice.toLocaleString()}đ</TableCell>
                                                        <TableCell align="right">{(element.quantity * element.product.promotionPrice).toLocaleString()}đ</TableCell>
                                                    </TableRow>
                                                )
                                            }) : null
                                        }
                                        <TableRow>
                                            <TableCell><b>TỔNG CỘNG</b></TableCell>
                                            <TableCell align="center"></TableCell>
                                            <TableCell align="center"></TableCell>
                                            <TableCell align="right"><b>{totalPayment.toLocaleString()}đ</b></TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                            <Grid container mt={2} spacing={1} justifyContent="end">
                                <Grid item>
                                    <Button variant="contained" color="inherit" onClick={() => dispatch(orderDetailCloseAction())}>Quay lại</Button>
                                </Grid>
                                <Grid item>
                                    <Button 
                                        variant="contained" color="success" 
                                        onClick={btnUpdateOrderHandler}
                                        disabled={currentOrderStatus === "Delivered" || currentOrderStatus === "Canceled" ? true : false}
                                    >
                                        Cập Nhật
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Modal>
                </Paper> : null
            }
        </>
    )
}

export default OrderDetailModal;