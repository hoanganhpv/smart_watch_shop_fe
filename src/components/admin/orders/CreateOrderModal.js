import { Autocomplete, Box, Button, FormControl, Grid, IconButton, InputLabel, MenuItem, Modal, Select, TextField, Tooltip, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {
    adminAddressChangeAction,
    adminCityChangeAction,
    adminCreateOrderRestFormAction,
    adminDistrictChangeAction,
    adminEmailChangeAction,
    adminFullnameChangeAction,
    adminNoteChangeAction,
    adminPhoneChangeAction,
    adminUsernameChangeAction,
    adminWardChangeAction,
    checkAdminOrderInfoAndCreateOrderAction,
    closeCreateOrderModalAction,
    getUserInforByEmailAction,
    getUserInforByPhoneAction,
    getUserInforByUsernameAction
} from "../../../actions/user.action";
import createAxiosInterceptorJWT from "../../../createAxiosJWT";
import PersonSearchIcon from '@mui/icons-material/PersonSearch';
import SelectProduct from "./SelectProduct";
import CreateOrderTableProduct from "./CreateOrderTableProduct";
import { useEffect, useState } from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1200,
    bgcolor: 'background.paper',
    p: 3,
    borderRadius: 2
};

const CreateOrderModal = () => {
    const { userData, createOrderModalOpen, adminCreateOrderInfor, adminOrderAddressList, adminCartProduct } = useSelector(reduxData => reduxData.userReducer);
    // user list state
    const [userList, setUserList] = useState([]);
    const [emailList, setEmailList] = useState([]);
    const [phoneList, setPhoneList] = useState([]);
    // user list Autocomplete input
    const dispatch = useDispatch();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);

    useEffect(() => {
        // get user list
        const getUserList = async () => {
            try {
                let userList = [];
                let emailList = [];
                let phoneList = [];
                // set URL api get all orders:
                const getCustomerListURL = "http://localhost:8000/customers";
                // apply access token to headers
                const axiosOption = {
                    headers: {
                        "Token": `Bearer ${userData.accessToken}`
                    },
                };
                // start call api
                const { data } = await axiosJWT.get(getCustomerListURL, axiosOption);
                // set data for username list, email list, phone list
                await data.forEach(element => userList.push(element.user.username));
                await data.forEach(element => phoneList.push(element.phone));
                await data.forEach(element => emailList.push(element.email));
                setUserList(userList);
                setEmailList(emailList);
                setPhoneList(phoneList);
            }
            catch (err) {
                console.log(err);
            }
        }
        // run function get customer list
        getUserList();
        // eslint-disable-next-line
    }, []);

    // handle button check username
    const btnCheckUsernameHandler = () => {
        const usernameValue = adminCreateOrderInfor.username;
        dispatch(getUserInforByUsernameAction(userData, usernameValue, axiosJWT));
    }

    // handle button check email
    const btnCheckEmailHandler = () => {
        const emailValue = adminCreateOrderInfor.email;
        dispatch(getUserInforByEmailAction(userData, emailValue, axiosJWT));
    }

    // handle button check phone number
    const btnCheckPhoneHandler = () => {
        const phoneValue = adminCreateOrderInfor.phone;
        dispatch(getUserInforByPhoneAction(userData, phoneValue, axiosJWT));
    }

    // handle button Create new order
    const createNewOrderHandler = () => {
        dispatch(checkAdminOrderInfoAndCreateOrderAction(userData, adminCartProduct, axiosJWT));
    }

    const handleCityChange = (event) => {
        const city = event.target.value;
        // Lọc lấy danh sách Quận/Huyện theo Tỉnh/Thành phố đã chọn
        const districtList = adminOrderAddressList.cityList.filter(element => element.name === city)[0].districts;
        dispatch(adminCityChangeAction(city, districtList));

        // Nếu thay đổi Tỉnh/thành Phố thì xóa giá trị ở 2 ô Quận/Huyện và Phường/Xã
        dispatch(adminDistrictChangeAction(""));
        dispatch(adminWardChangeAction(""));
    };

    const handleDistrictChange = (event) => {
        const district = event.target.value;
        // Lọc lấy danh sách Phường/Xã theo Quận/Huyện đã chọn
        const wardList = adminOrderAddressList.districtList.filter(element => element.name === district)[0].wards;
        dispatch(adminDistrictChangeAction(district, wardList));

        // Nếu thay đổi Quận/Huyện thì xóa giá trị ở ô Phường/Xã
        dispatch(adminWardChangeAction(""));
    };

    const handleWardChange = (event) => {
        const ward = event.target.value;
        dispatch(adminWardChangeAction(ward));
    };

    return (
        <Modal open={createOrderModalOpen} onClose={() => dispatch(closeCreateOrderModalAction())}>
            <Box sx={style}>
                <Typography textAlign="center" variant="h5" fontWeight={600}>TẠO ĐƠN HÀNG MỚI</Typography>
                <Grid container mt={0} spacing={3}>
                    <Grid item xs={12} sm={4}>
                        <Typography variant="h6">Thông tin khách hàng:</Typography>
                        {/* USERNAME autocomplete input and check button */}
                        <Grid container spacing={1} alignItems="center">
                            <Grid item xs={10}>
                                <Autocomplete
                                    fullWidth
                                    value={adminCreateOrderInfor.username}
                                    onChange={(event, newValue) => {
                                        dispatch(adminUsernameChangeAction(newValue))
                                    }}
                                    options={userList}
                                    renderInput={(params) => <TextField {...params} label="Tên Đăng Nhập" margin="dense" size="small" fullWidth />}
                                />
                            </Grid>
                            <Grid item xs={2} textAlign="right">
                                <Tooltip title="Kiểm Tra">
                                    <IconButton onClick={btnCheckUsernameHandler}><PersonSearchIcon color="success" /></IconButton>
                                </Tooltip>
                            </Grid>
                        </Grid>

                        {/* EMAIL autocomplete input and check button */}
                        <Grid container spacing={1} alignItems="center">
                            <Grid item xs={10}>
                                <Autocomplete
                                    fullWidth
                                    value={adminCreateOrderInfor.email}
                                    onChange={(event, newValue) => {
                                        dispatch(adminEmailChangeAction(newValue))
                                    }}
                                    options={emailList}
                                    renderInput={(params) => <TextField {...params} label="Email" margin="dense" size="small" fullWidth />}
                                />
                            </Grid>
                            <Grid item xs={2} textAlign="right">
                                <Tooltip title="Kiểm Tra">
                                    <IconButton onClick={btnCheckEmailHandler}><PersonSearchIcon color="success" /></IconButton>
                                </Tooltip>
                            </Grid>
                        </Grid>

                        {/* PHONE autocomplete input and check button */}
                        <Grid container spacing={1} alignItems="center">
                            <Grid item xs={10}>
                                <Autocomplete
                                    fullWidth
                                    value={adminCreateOrderInfor.phone}
                                    onChange={(event, newValue) => {
                                        dispatch(adminPhoneChangeAction(newValue))
                                    }}
                                    options={phoneList}
                                    renderInput={(params) => <TextField {...params} label="Số Điện Thoại" margin="dense" size="small" fullWidth />}
                                />
                            </Grid>
                            <Grid item xs={2} textAlign="right">
                                <Tooltip title="Kiểm Tra">
                                    <IconButton onClick={btnCheckPhoneHandler}><PersonSearchIcon color="success" /></IconButton>
                                </Tooltip>
                            </Grid>
                        </Grid>

                        <TextField
                            value={adminCreateOrderInfor.fullName}
                            onChange={(e) => dispatch(adminFullnameChangeAction(e.target.value))}
                            label="Họ Và Tên" fullWidth size="small" margin="dense"
                        />

                        <TextField
                            value={adminCreateOrderInfor.address}
                            onChange={(e) => dispatch(adminAddressChangeAction(e.target.value))}
                            label="Địa Chỉ" fullWidth size="small" margin="dense"
                        />

                        <FormControl fullWidth size="small" margin="dense">
                            <InputLabel>Thành Phố</InputLabel>
                            <Select
                                value={adminCreateOrderInfor.city}
                                label="Thành phố"
                                onChange={handleCityChange}
                            >
                                {adminOrderAddressList.cityList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>)}
                            </Select>
                        </FormControl>

                        <FormControl fullWidth size="small" margin="dense">
                            <InputLabel>Quận/Huyện</InputLabel>
                            <Select
                                value={adminCreateOrderInfor.district}
                                label="Quận/Huyện"
                                onChange={handleDistrictChange}
                            >
                                {adminOrderAddressList.districtList ? adminOrderAddressList.districtList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                            </Select>
                        </FormControl>

                        <FormControl fullWidth size="small" margin="dense">
                            <InputLabel>Phường/Xã</InputLabel>
                            <Select
                                value={adminCreateOrderInfor.ward}
                                label="Phường/Xã"
                                onChange={handleWardChange}
                            >
                                {adminOrderAddressList.wardList ? adminOrderAddressList.wardList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                            </Select>
                        </FormControl>

                        <TextField
                            value={adminCreateOrderInfor.note}
                            onChange={(e) => dispatch(adminNoteChangeAction(e.target.value))}
                            label="Ghi Chú" multiline minRows={2} fullWidth size="small" margin="dense"
                        />
                    </Grid>

                    <Grid item xs={12} sm={8}>
                        <Typography variant="h6">Thêm sản phẩm:</Typography>
                        <SelectProduct />

                        <Typography mt={2} variant="h6">Chi tiết:</Typography>
                        <CreateOrderTableProduct />
                    </Grid>
                </Grid>

                <Grid container mt={2} spacing={1} justifyContent="end">
                    <Grid item>
                        <Button variant="contained" color="inherit" onClick={() => dispatch(closeCreateOrderModalAction())}>Quay lại</Button>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" color="error" onClick={() => dispatch(adminCreateOrderRestFormAction())}>Xóa Toàn Bộ Thông Tin</Button>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" color="success" onClick={createNewOrderHandler}>Tạo Đơn Hàng</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default CreateOrderModal;