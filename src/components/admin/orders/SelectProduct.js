import { Autocomplete, Grid, IconButton, TextField, Tooltip } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProductsAction } from "../../../actions/product.action";
import { addProductToAdminCartAction, addProductToAdminCartEmptyNameAction } from "../../../actions/user.action";

const SelectProduct = () => {
    const { allProducts } = useSelector(reduxData => reduxData.productReducer);
    const { adminCartProduct } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const [product, setProduct] = useState(null);
    // state of product name and product quantity
    const [inputValue, setInputValue] = useState('');
    const [quantity, setQuantity] = useState(1);

    // get all product when open modal
    useEffect(() => {
        dispatch(getAllProductsAction())
    }, [dispatch]);

    const options = [];
    allProducts.forEach(element => options.push(element.name));
    options.sort();

    // function get product: input is product name; return output is product data object
    const getProduct = (productName) => {
        return allProducts.find(element => element.name === productName);
    }

    // quantity change handler:
    const quantityChangeHandler = (event) => {
        const value = parseInt(event.target.value);
        // if quantity < 1: set quantity = 1 and return
        if (value < 1) {
            setQuantity(1);
            return;
        }
        // else set product quantity = value
        setQuantity(value);
    }

    // add product button handler
    const addProductHandler = () => {
        if (!product) {
            dispatch(addProductToAdminCartEmptyNameAction());
        }
        else {
            const productData = getProduct(product);
            const cartData = {
                id: productData._id,
                name: product,
                quantity: quantity,
                promotionPrice: productData.promotionPrice
            };
            dispatch(addProductToAdminCartAction(adminCartProduct, cartData));
        }
        // set quality back to default number 1
        setQuantity(1);
    }

    return (
        <div>
            <Grid container spacing={1} alignItems="center">
                <Grid item xs={9}>
                    <Autocomplete
                        fullWidth
                        value={product}
                        onChange={(event, newValue) => {
                            setProduct(newValue);
                        }}
                        inputValue={inputValue}
                        onInputChange={(event, newInputValue) => {
                            setInputValue(newInputValue);
                        }}
                        options={options}
                        renderInput={(params) => {
                            return (
                                <>
                                    <TextField {...params} label="Sản Phẩm" margin="dense" size="small" fullWidth />
                                </>
                            )
                        }}
                    />
                </Grid>
                <Grid item xs={2}>
                    <TextField value={quantity} onChange={quantityChangeHandler} type="number" label="Số Lượng" size="small" margin="dense" fullWidth />
                </Grid>
                <Grid item xs={1} textAlign="right">
                    <Tooltip title="Thêm sản phẩm">
                        <IconButton sx={{ mt: "3px" }} color="success" onClick={addProductHandler}><AddCircleIcon /></IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
        </div>
    )
}

export default SelectProduct;