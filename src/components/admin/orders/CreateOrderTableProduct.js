import { Grid, IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, Typography } from "@mui/material";
import ClearIcon from '@mui/icons-material/Clear';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import ControlPointIcon from '@mui/icons-material/ControlPoint';
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { decreaseProductQuantityAction, increaseProductQuantityAction } from "../../../actions/user.action";
import cartEmptyImg from "../../../assets/images/cart_empty.png";

const CreateOrderTableProduct = () => {
    const { adminCartProduct } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const [totalPrice, setTotalPrice] = useState(0);

    // calculate total price of order
    let total = 0;
    adminCartProduct.forEach(element => total += (element.promotionPrice * element.quantity));
    useEffect(() => {
        setTotalPrice(total);
    }, [total]);

    // handle increase quantity button
    const increaseProductQuantityHandler = (index) => {
        dispatch(increaseProductQuantityAction(adminCartProduct, index));
    }

    // handle decrease quantity button
    const decreaseProductQuantityHandler = (index) => {
        // if product quantity = 1: stop
        if (adminCartProduct[index].quantity === 1) {
            return;
        }
        else {
            dispatch(decreaseProductQuantityAction(adminCartProduct, index));
        }
    }

    return (
        adminCartProduct.length > 0
            ?
            <TableContainer component={Paper}>
                <Table size="small" aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Sản Phẩm</TableCell>
                            <TableCell align="center">Số Lượng</TableCell>
                            <TableCell align="right">Thành Tiền</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            adminCartProduct ? adminCartProduct.map((element, index) => (
                                <TableRow key={index} >
                                    <TableCell>{element.name}</TableCell>
                                    <TableCell align="center">
                                        <IconButton onClick={() => decreaseProductQuantityHandler(index)}><RemoveCircleOutlineIcon fontSize="small" /></IconButton>
                                        {element.quantity}
                                        <IconButton onClick={() => increaseProductQuantityHandler(index)}><ControlPointIcon fontSize="small" /></IconButton>
                                    </TableCell>
                                    <TableCell align="right">{(element.promotionPrice * element.quantity).toLocaleString()}đ</TableCell>
                                    <TableCell>
                                        <Tooltip title="Xóa">
                                            <IconButton color="error"><ClearIcon fontSize="small" /></IconButton>
                                        </Tooltip>
                                    </TableCell>
                                </TableRow>
                            )) : null
                        }
                        <TableRow>
                            <TableCell><b>TỔNG CỘNG</b></TableCell>
                            <TableCell></TableCell>
                            <TableCell align="right"><b>{totalPrice.toLocaleString()}đ</b></TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
            :
            <>
                <Grid container mt={3} justifyContent="center" alignItems="center">
                    <img src={cartEmptyImg} width="200px" alt="cart empty" />
                </Grid>
                <Typography textAlign="center" variant="h5" fontWeight={600}>GIỎ HÀNG RỖNG</Typography>
                <Typography textAlign="center" variant="h6">Thêm sản phẩm để tạo đơn hàng mới</Typography>
            </>
    )
}

export default CreateOrderTableProduct;