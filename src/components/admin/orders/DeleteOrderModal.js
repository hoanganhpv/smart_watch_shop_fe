import { Box, Button, Grid, Modal, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { deleteOrderModalCloseAction, orderDeleteConfirmAction } from "../../../actions/user.action";
import createAxiosInterceptorJWT from "../../../createAxiosJWT";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    p: 3,
    borderRadius: 2
};

const DeleteOrderModal = () => {
    const { userData, deleteModalOpen, deleteOrderId } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const axiosJWT = createAxiosInterceptorJWT(dispatch);

    // handle close modal
    const closeDeleteOrderModalHandler = () => {
        dispatch(deleteOrderModalCloseAction());
    }

    // handle button delete order click
    const btnDeleteOrderHandler = () => {
        dispatch(orderDeleteConfirmAction(userData, deleteOrderId, axiosJWT));
    }

    return (
        <Modal open={deleteModalOpen} onClose={closeDeleteOrderModalHandler} >
            <Box sx={style}>
                <Typography variant="h5" component="h2">
                    XÓA ĐƠN HÀNG
                </Typography>
                <Typography sx={{ mt: 4 }}>
                    Bạn chắc chắn muốn xóa đơn hàng này?
                </Typography>
                <Grid container justifyContent="end" mt={3} spacing={1}>
                    <Grid item>
                        <Button color="inherit" variant="contained" onClick={closeDeleteOrderModalHandler}>Quay lại</Button>
                    </Grid>
                    <Grid item>
                        <Button color="error" variant="contained" onClick={btnDeleteOrderHandler}>Xóa Đơn Hàng</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default DeleteOrderModal;