import { Breadcrumbs, Link } from "@mui/material";

const BreadCrumb = (props) => {
    const { address } = props;

    return (
        <Breadcrumbs separator="›" aria-label="breadcrumb" sx={{fontSize: 15}}>
            {
                address.map((element, index) => {
                    return <Link key={index} href={element.url} underline="hover" color="black"><b>{element.name}</b></Link>
                })
            }
        </Breadcrumbs>
    )
}

export default BreadCrumb;