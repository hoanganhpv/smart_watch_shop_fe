import { Alert, Snackbar } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { closeSnackbarErrorAction, closeSnackbarSuccessAction } from "../../actions/product.action";
import { closeUserErrorAlertAction, closeUserSuccessAlertAction } from "../../actions/user.action";

const SnackbarAlert = () => {
    const { snackbarSuccess, snackbarError } = useSelector(reduxData => reduxData.productReducer);
    const { userSuccessAlert, userErrorAlert } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    // PRODUCT RELEVANT ALERT
    const handleCloseSuccess = () => {
        dispatch(closeSnackbarSuccessAction());
    };
    const handleCloseError = () => {
        dispatch(closeSnackbarErrorAction());
    };

    // USER RELEVANT ALERT
    const handleCloseUserAlertSuccess = () => {
        dispatch(closeUserSuccessAlertAction());
    }
    const handleCloseUserAlertError = () => {
        dispatch(closeUserErrorAlertAction());
    }

    return (
        <>
            <Snackbar open={snackbarSuccess.show} autoHideDuration={6000} onClose={handleCloseSuccess} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert onClose={handleCloseSuccess} variant="filled" severity="success" sx={{ width: '100%' }}>
                    {snackbarSuccess.message}
                </Alert>
            </Snackbar>

            <Snackbar open={snackbarError.show} autoHideDuration={6000} onClose={handleCloseError} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert onClose={handleCloseError} variant="filled" severity="error" sx={{ width: '100%' }}>
                    {snackbarError.message}
                </Alert>
            </Snackbar>

            <Snackbar open={userSuccessAlert.show} autoHideDuration={6000} onClose={handleCloseUserAlertSuccess} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert onClose={handleCloseUserAlertSuccess} variant="filled" severity="success" sx={{ width: '100%' }}> 
                    {userSuccessAlert.message}
                </Alert>
            </Snackbar>

            <Snackbar open={userErrorAlert.show} autoHideDuration={6000} onClose={handleCloseUserAlertError} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Alert onClose={handleCloseUserAlertError} variant="filled" severity="error" sx={{ width: '100%' }}>
                    {userErrorAlert.message}
                 </Alert>
            </Snackbar>
        </>
    )
}

export default SnackbarAlert;