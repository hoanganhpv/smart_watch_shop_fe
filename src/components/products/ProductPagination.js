import { Pagination } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { paginationChangeAciton } from "../../actions/product.action";

const ProductPagination = () => {
    const dispatch = useDispatch();
    const { currentPage, totalPage } = useSelector(reduxData => reduxData.productReducer);

    const paginationChangeHandler = (event, value) => {
        dispatch(paginationChangeAciton(value));
    }

    return <Pagination variant="outlined" count={totalPage} page={currentPage} onChange={paginationChangeHandler} />
}

export default ProductPagination;