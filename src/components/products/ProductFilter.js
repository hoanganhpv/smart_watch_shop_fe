import { Box, Button, Checkbox, FormControlLabel, FormGroup, Grid, TextField, ThemeProvider, Typography, createTheme } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { brandFilterChangeAction, btnFilterClickAction, getProductDataAction, maxPriceFilterChangeAction, minPriceFilterChangeAction, nameFilterChangeAction } from "../../actions/product.action";

const theme = createTheme({
    palette: {
        info: {
            main: "#000000",
        },
    },
});

const ProductFilter = () => {
    const dispatch = useDispatch();
    const { currentPage, limit, filterName, filterMinPrice, filterMaxPrice, filterBrand } = useSelector(reduxData => reduxData.productReducer);

    // funciton xử lý sự kiện onChange mục Lọc theo tên sản phẩm
    const nameFilterChangeHandler = (event) => {
        const value = event.target.value;
        dispatch(nameFilterChangeAction(value));
    }

    // funciton xử lý sự kiện onChange mục Lọc theo min price
    const minPriceChangeHandler = (event) => {
        dispatch(minPriceFilterChangeAction(event.target.value));
    }

    // funciton xử lý sự kiện onChange mục Lọc theo max price
    const maxPriceChangeHandler = (event) => {
        dispatch(maxPriceFilterChangeAction(event.target.value));
    }

    // funciton xử lý sự kiện onChange mục Lọc theo brand (checkbox)
    const brandCheckboxHandler = (event) => {
        const value = event.target.value;
        const isChecked = event.target.checked;
        dispatch(brandFilterChangeAction(value, isChecked));
    }

    const filterHandler = () => {
        dispatch(btnFilterClickAction());
        dispatch(getProductDataAction(currentPage, limit, filterName, filterMinPrice, filterMaxPrice, filterBrand));
    }

    return (
        <Box>
            <Grid container>
                <Grid item xs={12}>
                    <Typography variant="body1" fontWeight={600}>Lọc Sản Phẩm:</Typography>
                </Grid>
            </Grid>
            <Grid container mt={2} spacing={1}>
                <Grid item xs={12}>
                    <TextField label="Tên Sản Phẩm" value={filterName} onChange={nameFilterChangeHandler} size="small" fullWidth />
                </Grid>
            </Grid>
            <Grid container mt={2} spacing={1} alignItems="center">
                <Grid item xs={5}>
                    <TextField label="Giá tối thiểu" value={filterMinPrice} onChange={minPriceChangeHandler} fullWidth size="small" />
                </Grid>
                <Grid item xs={2}>
                    <Typography textAlign="center" variant="h5">-</Typography>
                </Grid>
                <Grid item xs={5}>
                    <TextField label="Giá tối đa" value={filterMaxPrice} onChange={maxPriceChangeHandler} fullWidth size="small" />
                </Grid>
            </Grid>
            <Grid container mt={2} spacing={1} alignItems="center">
                <Grid item xs={12}>
                    <FormGroup>
                        <FormControlLabel value="642bc3ef1a7c6b7e721cf34a" control={<Checkbox />} onChange={brandCheckboxHandler} label="APPLE" />
                        <FormControlLabel value="642bc3fa1a7c6b7e721cf34c" control={<Checkbox />} onChange={brandCheckboxHandler} label="SAMSUNG" />
                        <FormControlLabel value="642bc4021a7c6b7e721cf34e" control={<Checkbox />} onChange={brandCheckboxHandler} label="GARMIN" />
                    </FormGroup>
                </Grid>
            </Grid>
            <Grid container mt={2}>
                <Grid item xs={12}>
                    <ThemeProvider theme={theme}>
                        <Button variant="outlined" color="info" onClick={filterHandler} fullWidth>Lọc Sản Phẩm</Button>
                    </ThemeProvider>
                </Grid>
            </Grid>
        </Box>
    )
}

export default ProductFilter;