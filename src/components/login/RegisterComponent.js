import { Box, Button, Card, CardContent, Paper, TextField, ThemeProvider, Typography, createTheme, Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { checkRegisterAndCreateUserAction, registerEmailChangeAction, registerFullnameChangeAction, registerPasswordChangeAction, registerPhoneChangeAction, registerRepeatPasswordChangeAction, registerUsernameChangeAction, userRegisterFalseAction } from "../../actions/user.action";
import AddressSelect from "./AddressSelect";

const style = {
    minHeight: '470px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
}

const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const RegisterComponent = () => {
    const { registerData } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    // Back to Login Button handler
    const btnBackToLoginHandler = () => {
        dispatch(userRegisterFalseAction());
    }

    // Register User Button handler:
    const registerHandler = () => {
        dispatch(checkRegisterAndCreateUserAction());
    }

    return (
        <Box sx={style}>
            <Paper elevation={3} sx={{ mt: 2, borderRadius: 2 }}>
                <Card sx={{ p: 2, borderRadius: 2 }}>
                    <CardContent>
                        <Typography letterSpacing={2} textAlign="center" fontWeight={600} variant="h5">ĐĂNG KÝ</Typography>

                        <Typography mt={3} fontSize={15} fontWeight={500} >Thông tin đăng nhập</Typography>
                        <TextField 
                            size="small" label="Tên đăng nhập" 
                            value={registerData.username} 
                            onChange={(e) => dispatch(registerUsernameChangeAction(e.target.value))} 
                            fullWidth margin="dense" 
                        />
                        <TextField 
                            size="small" label="Mật khẩu" type="password" 
                            value={registerData.password} 
                            onChange={(e) => dispatch(registerPasswordChangeAction(e.target.value))} 
                            fullWidth margin="dense" 
                        />
                        <TextField 
                            size="small" label="Nhập lại mật khẩu" type="password" 
                            value={registerData.repeatPassword} 
                            onChange={(e) => dispatch(registerRepeatPasswordChangeAction(e.target.value))} 
                            fullWidth margin="dense" 
                        />

                        <Typography mt={3} fontSize={15} fontWeight={500} >Thông tin giao hàng</Typography>
                        <TextField 
                            size="small" label="Họ và tên" 
                            value={registerData.fullName} 
                            onChange={(e) => dispatch(registerFullnameChangeAction(e.target.value))} 
                            fullWidth margin="dense" 
                        />
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <TextField 
                                    size="small" label="Email" 
                                    value={registerData.email} 
                                    onChange={(e) => dispatch(registerEmailChangeAction(e.target.value))} 
                                    fullWidth margin="dense" />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField 
                                    size="small" label="Số điện thoại" 
                                    value={registerData.phone} 
                                    onChange={(e) => dispatch(registerPhoneChangeAction(e.target.value))} 
                                    fullWidth margin="dense" />
                            </Grid>
                        </Grid>

                        <AddressSelect />

                        <ThemeProvider theme={theme}>
                            <Button sx={{ mt: 3 }} variant="contained" color="info" size="large" fullWidth onClick={registerHandler}>Đăng ký tài khoản</Button>
                        </ThemeProvider>
                        <Button sx={{ mt: 2 }} variant="contained" color="inherit" size="large" fullWidth onClick={btnBackToLoginHandler}>Quay lại trang đăng nhập</Button>
                    </CardContent>
                </Card>
            </Paper>
        </Box>
    )
}

export default RegisterComponent;