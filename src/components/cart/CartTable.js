import { Grid, IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import ClearIcon from '@mui/icons-material/Clear';
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { productCartUpdateAction, removeFromCartSuccess } from "../../actions/product.action";
import { Link } from "react-router-dom";

const CartTable = () => {
    const [cartProduct, setCartProduct] = useState([]);
    const [cartTotalPrice, setCartTotalPrice] = useState(0);
    const [cartTotalProduct, setCartTotalProduct] = useState(0);
    const dispatch = useDispatch();

    useEffect(() => {
        // lấy dữ liệu từ local storage
        const localStorageCart = JSON.parse(localStorage.getItem("cart")) || [];

        // thêm phương thức tính tổng tiền cho mỗi sản phẩm trong giỏ hàng theo giá bán và số lượng
        localStorageCart.map(element => {
            return element.totalPrice = () => {
                return element.promotionPrice * element.quantity;
            }
        });

        // tính tổng tiền của đơn hàng
        let totalPrice = 0;
        localStorageCart.forEach(element => {
            totalPrice += element.totalPrice();
        });

        // tính tổng sản phẩm của đơn hàng
        let totalProduct = 0;
        localStorageCart.forEach(element => {
            totalProduct += element.quantity;
        });

        // lưu data trên vào state để xử lý react
        setCartProduct(localStorageCart);
        setCartTotalPrice(totalPrice);
        setCartTotalProduct(totalProduct);
    }, []);

    // xử lý click - số lượng
    const minusNumberHandler = (product, index) => {
        if (product.quantity === 1) {
            return;
        }
        setCartProduct(previousData => {
            // copy state cũ
            let newData = [...previousData];
            newData[index].quantity -= 1;
            // update lại đơn hàng trên local storage
            localStorage.setItem("cart", JSON.stringify(newData));
            return newData;
        });
        // thay đổi state tổng tiền đơn hàng
        setCartTotalPrice(cartTotalPrice - product.promotionPrice);
        setCartTotalProduct(cartTotalProduct - 1);
    }

    // xử lý click + số lượng
    const plusNumberHandler = (product, index) => {
        setCartProduct(previousData => {
            // copy state cũ
            let newData = [...previousData];
            newData[index].quantity += 1;
            // update lại đơn hàng trên local storage
            localStorage.setItem("cart", JSON.stringify(newData));
            return newData;
        });
        // thay đổi state tổng tiền đơn hàng
        setCartTotalPrice(cartTotalPrice + product.promotionPrice);
        setCartTotalProduct(cartTotalProduct + 1);
    }

    // xử lý click button Xóa sản phẩm
    const removeProductHandler = (product, index) => {
        setCartProduct(previousData => {
            // copy state cũ
            let newData = [...previousData];
            newData.splice(index, 1);
            // lưu giá trị mới vào local storage
            localStorage.setItem("cart", JSON.stringify(newData));
            // dispatch cart number về reducer
            dispatch(productCartUpdateAction(newData.length));
            return newData;
        })
        // thay đổi state tổng tiền đơn hàng
        setCartTotalPrice(cartTotalPrice - product.totalPrice());
        setCartTotalProduct(cartTotalProduct - product.quantity);
        // hiện snackbar alert xóa thành công
        dispatch(removeFromCartSuccess());
    }

    return (
        <div>
            <Paper sx={{ mt: 2 }} elevation={2}>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{ fontSize: 16 }}><b>Sản Phẩm</b></TableCell>
                                <TableCell sx={{ fontSize: 16 }} align="center"><b>Giá Bán</b></TableCell>
                                <TableCell sx={{ fontSize: 16 }} align="center"><b>Số Lượng</b></TableCell>
                                <TableCell sx={{ fontSize: 16 }} align="center"><b>Thành Tiền</b></TableCell>
                                <TableCell sx={{ fontSize: 16 }} align="center"><b>Xóa</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {cartProduct.map((element, index) => (
                                <TableRow
                                    key={index}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell>
                                        <img className="cart-img" src={element.imageUrl} alt="product" />
                                        <Link to={`/products/${element._id}`}><b>{element.name}</b></Link>
                                    </TableCell>
                                    <TableCell align="center">{element.promotionPrice.toLocaleString("en-us")}đ</TableCell>
                                    <TableCell>
                                        <Grid container justifyContent="center" alignItems="center">
                                            <Grid item>
                                                <IconButton onClick={() => minusNumberHandler(element, index)}><RemoveCircleIcon /></IconButton>
                                            </Grid>
                                            <Grid item sx={{ mx: 1 }}>
                                                {element.quantity}
                                            </Grid>
                                            <Grid item>
                                                <IconButton onClick={() => plusNumberHandler(element, index)}><AddCircleIcon /></IconButton>
                                            </Grid>
                                        </Grid>
                                    </TableCell>
                                    <TableCell align="center">{element.totalPrice().toLocaleString("en-us")}đ</TableCell>
                                    <TableCell align="center">
                                        <IconButton onClick={() => removeProductHandler(element, index)}><ClearIcon sx={{ fontSize: 18 }} color="error" /></IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                            <TableRow sx={{ borderTop: "2px solid grey" }}>
                                <TableCell sx={{ fontSize: 18, fontWeight: 600 }}>TỔNG CỘNG</TableCell>
                                <TableCell></TableCell>
                                <TableCell sx={{ fontSize: 18, fontWeight: 600 }} align="center">{cartTotalProduct.toLocaleString("en-us")}</TableCell>
                                <TableCell sx={{ fontSize: 18, fontWeight: 600 }} align="center">{cartTotalPrice.toLocaleString("en-us")}đ</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    )
}

export default CartTable;