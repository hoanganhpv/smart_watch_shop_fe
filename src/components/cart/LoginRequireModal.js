import { Box, Button, Grid, Modal, ThemeProvider, Typography, createTheme } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { closeLoginRequireModalAction } from "../../actions/user.action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    borderRadius: 2,
    boxShadow: 24,
    p: 2,
};

const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const LoginRequireModal = () => {
    const { loginRequireModalOpen } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleClose = () => {
        dispatch(closeLoginRequireModalAction());
    }

    const loginButtonHandler = () => {
        dispatch(closeLoginRequireModalAction());
        navigate("/login");
    }
    return (
        <Modal open={loginRequireModalOpen} onClose={handleClose}>
            <Box sx={style}>
                <Typography variant="h5" component="h2">
                    Yêu cầu đăng nhập
                </Typography>
                <Typography sx={{ mt: 4 }}>
                    Vui lòng đăng nhập để hoàn tất đơn hàng.
                </Typography>
                <Grid container justifyContent="end" mt={3} spacing={1}>
                    <Grid item>
                        <Button color="inherit" variant="contained" onClick={handleClose}>Quay lại</Button>
                    </Grid>
                    <Grid item>
                        <ThemeProvider theme={theme}>
                            <Button color="info" variant="contained" onClick={loginButtonHandler}>Đăng nhập</Button>
                        </ThemeProvider>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default LoginRequireModal;