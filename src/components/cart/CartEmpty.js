import { Button, createTheme, Grid, ThemeProvider, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import cartEmpty from "../../assets/images/cart_empty.png";

const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const CartEmpty = () => {
    const navigate = useNavigate();

    return (
        <Grid container mt={2} alignItems="center" justifyContent="center">
            <Grid item xs={12} textAlign="center">
                <img src={cartEmpty} width="250" alt="empty-cart" />
            </Grid>
            <Grid item xs={12} textAlign="center">
                <Typography mt={2} variant="h4">GIỎ HÀNG RỖNG</Typography>
                <Typography mt={2} variant="h5">Thêm sản phẩm ngay để nhận ưu đãi giảm giá đến 50%</Typography>
                <ThemeProvider theme={theme}>
                    <Button sx={{mt: 2}} variant="contained" color="info" onClick={() => navigate("/products")}>Tất cả sản phẩm</Button>
                </ThemeProvider>
            </Grid>
        </Grid>
    )
}

export default CartEmpty;