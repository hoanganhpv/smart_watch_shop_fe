import { Grid} from "@mui/material";
import { useEffect, useState } from "react";
import ProductCard4 from "../products/ProductCard4";

const RelatedProduct = (props) => {
    const { productDetail } = props;
    const [ relatedProduct, setRelatedProduct ] = useState(null);

    useEffect(() => {
        const getRelatedProduct = async () => {
            const response = await fetch("http://127.0.0.1:8000/products?brand=" + productDetail.type);
            const jsonData = await response.json();
            // remove current product from data to show:
            const displayData = await jsonData.data.filter(element => element.name !== productDetail.name);
            setRelatedProduct(displayData);
        }
        getRelatedProduct();
    }, [productDetail]);

    return (
        <>
            <Grid container mt={3}>
                <Grid item xs={12}>
                    <h1 className="title-text">Sản Phẩm Liên Quan</h1>
                </Grid>
            </Grid>

            <Grid container spacing={2} my={3} className="lastest-product">
                { relatedProduct ? 
                relatedProduct.map((element, index) => {
                    return (
                        <ProductCard4 key={index} data={element} />
                    )
                }) : null
                }
            </Grid>
        </>
    )
}

export default RelatedProduct;