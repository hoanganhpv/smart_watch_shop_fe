import { Button, Container, Grid, IconButton, ThemeProvider, Typography, createTheme } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addToCartSuccessAction, productCartUpdateAction } from "../../actions/product.action";


const theme = createTheme({
    palette: {
        info: {
            main: "#404040",
        },
    },
});

const ProductInfo = (props) => {
    const { productDetail } = props;
    const dispatch = useDispatch();
    const [number, setNumber] = useState(1);
    const [brand, setBrand] = useState("");

    useEffect(() => {
        const getBrand = async () => {
            const response = await fetch("http://127.0.0.1:8000/product_types/" + productDetail.type);
            const responseJson = await response.json();
            setBrand(responseJson.data.name);
        }
        getBrand();
        // eslint-disable-next-line
    }, [])

    // xử lý click -
    const minusNumberHandler = () => {
        if (number === 1) {
            return;
        }
        setNumber(number - 1);
    }
    // xử lý click +
    const plusNumberHandler = () => {
        setNumber(number + 1);
    }

    // xử lý click button Add to cart
    const addToCartHandler = () => {
        // khai báo đối tượng chứa dữ liệu cần lưu
        const product = {
            _id: productDetail._id,
            name: productDetail.name,
            quantity: number,
            promotionPrice: productDetail.promotionPrice,
            imageUrl: productDetail.imageUrl,
        };
        // lấy dữ liệu từ local storage, nếu chưa có dữ liệu sẽ trả về mảng rỗng
        const localData = JSON.parse(localStorage.getItem("cart")) || [];

        const index = localData.findIndex(element => element.name === product.name);
        // nếu sản phẩm đã tồn tại thì tăng số lượng sản phẩm
        if (index !== -1) {
            localData[index].quantity += product.quantity;
        }
        // nếu sản phẩm chưa tồn tại thì thêm sản phẩm mới vào giỏ hàng
        else {
            localData.push(product);
        }

        // dispatch data cart number về reducer
        dispatch(productCartUpdateAction(localData.length));
        dispatch(addToCartSuccessAction());

        // lưu dữ liệu vào local storage
        localStorage.setItem("cart", JSON.stringify(localData));
    }

    return (
        <Container>
            <Grid container alignItems="center" spacing={2} mt={3}>
                <Grid item xs={12} md={4} p={2}>
                    <img width="100%" style={{ borderRadius: 10 }} src={productDetail.imageUrl} alt="product-detail" />
                </Grid>
                <Grid item xs={12} md={8}>
                    <Typography variant="h5" fontWeight={700}>{productDetail.name}</Typography>
                    <Typography sx={{ mt: 2 }} variant="h6">Nhãn hiệu: {brand}</Typography>
                    <Typography sx={{ mt: 2, fontSize: 17 }} ><b>Thông tin sản phẩm: </b>{productDetail.description}</Typography>
                    <Typography sx={{ mt: 2 }} variant="h5">
                        <small style={{ fontSize: 16 }}><del>{productDetail.buyPrice.toLocaleString('en-US')}đ</del></small> &nbsp;
                        <b style={{ color: "red" }}>{productDetail.promotionPrice.toLocaleString('en-US')}đ</b>
                    </Typography>
                    <Grid container spacing={1} mt={1} alignItems="center" justifyContent={{ xs: "center", sm: "left" }}>
                        <Grid item>
                            <IconButton onClick={minusNumberHandler}><RemoveCircleIcon /></IconButton>
                        </Grid>
                        <Grid item>
                            <Typography>{number}</Typography>
                        </Grid>
                        <Grid item>
                            <IconButton onClick={plusNumberHandler}><AddCircleIcon /></IconButton>
                        </Grid>
                    </Grid>
                    <Grid container mt={2} alignItems="center" justifyContent={{ xs: "center", sm: "left" }}>
                        <ThemeProvider theme={theme}>
                            <Button color="info" variant="contained" onClick={addToCartHandler}>Thêm vào giỏ hàng</Button>
                        </ThemeProvider>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
}

export default ProductInfo;