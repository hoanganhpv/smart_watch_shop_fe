import { FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addressInputChangeAction, cityInputChangeAction, districtInputChangeAction, getUserAddressAction, wardInputChangeAction } from "../../actions/user.action";

const BillingAddressSelect = () => {
    const { userData, orderInformation, orderAddressList } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    // Load dữ liệu address vào ô thông tin khi tải trang
    useEffect(() => {
        if (userData) {
            dispatch(getUserAddressAction(userData));
        }
    }, [userData, dispatch]);

    const handleAddressChange = (event) => {
        dispatch(addressInputChangeAction(event.target.value));
    }

    const handleCityChange = (event) => {
        const city = event.target.value;
        // Lọc lấy danh sách Quận/Huyện theo Tỉnh/Thành phố đã chọn
        const districtList = orderAddressList.cityList.filter(element => element.name === city)[0].districts;
        dispatch(cityInputChangeAction(city, districtList));

        // Nếu thay đổi Tỉnh/thành Phố thì xóa giá trị ở 2 ô Quận/Huyện và Phường/Xã
        dispatch(districtInputChangeAction(""));
        dispatch(wardInputChangeAction(""));
    };

    const handleDistrictChange = (event) => {
        const district = event.target.value;
        // Lọc lấy danh sách Phường/Xã theo Quận/Huyện đã chọn
        const wardList = orderAddressList.districtList.filter(element => element.name === district)[0].wards;
        dispatch(districtInputChangeAction(district, wardList));

        // Nếu thay đổi Quận/Huyện thì xóa giá trị ở ô Phường/Xã
        dispatch(wardInputChangeAction(""));
    };

    const handleWardChange = (event) => {
        const ward = event.target.value;
        dispatch(wardInputChangeAction(ward));
    };

    return (
        <div>
            <TextField value={orderInformation.address} onChange={handleAddressChange} label="Địa chỉ" size="small" fullWidth margin="normal" />

            <FormControl fullWidth size="small" margin="normal">
                <InputLabel id="demo-simple-select-label">Thành Phố</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={orderInformation.city}
                    label="Thành phố"
                    onChange={handleCityChange}
                >
                    {orderAddressList.cityList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>)}
                </Select>
            </FormControl>

            <FormControl fullWidth size="small" margin="normal">
                <InputLabel id="demo-simple-select-label">Quận/Huyện</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={orderInformation.district}
                    label="Quận/Huyện"
                    onChange={handleDistrictChange}
                >
                    {orderAddressList.districtList ? orderAddressList.districtList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                </Select>
            </FormControl>

            <FormControl fullWidth size="small" margin="normal">
                <InputLabel id="demo-simple-select-label">Phường/Xã</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={orderInformation.ward}
                    label="Phường/Xã"
                    onChange={handleWardChange}
                >
                    {orderAddressList.wardList ? orderAddressList.wardList.map((element, index) => <MenuItem key={index} value={element.name}>{element.name}</MenuItem>) : null}
                </Select>
            </FormControl>

        </div>
    )
}

export default BillingAddressSelect;