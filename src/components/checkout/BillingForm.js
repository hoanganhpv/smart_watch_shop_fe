import { Button, FormControl, FormControlLabel, FormLabel, Grid, Paper, Radio, RadioGroup, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { emailInputChangeAction, getUserInforAction, messageInputChangeAction, nameInputChangeAction, phoneInputChangeAction, voucherInputChangeAction } from "../../actions/user.action";
import BillingAddressSelect from "./BillingAddressSelect";

const BillingForm = () => {
    const { userData, orderInformation } = useSelector(reduxData => reduxData.userReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        if (userData) {
            dispatch(getUserInforAction(userData));
        }
    }, [userData, dispatch]);

    // handle name input change
    const nameChangeHandler = (event) => {
        dispatch(nameInputChangeAction(event.target.value));
    }
    // handle phone input change
    const phoneChangeHandler = (event) => {
        dispatch(phoneInputChangeAction(event.target.value));
    }
    // handle email input change
    const emailChangeHandler = (event) => {
        dispatch(emailInputChangeAction(event.target.value));
    }
    // handle message input change
    const messageChangeHandler = (event) => {
        dispatch(messageInputChangeAction(event.target.value));
    }
    // handle voucher input change
    const voucherChangeHandler = (event) => {
        dispatch(voucherInputChangeAction(event.target.value));
    }

    return (
        <div>
            <h1 style={{ textAlign: "center" }} className="title-text">Thông tin</h1>
            <Paper sx={{ p: 2 }} elevation={2}>
                <TextField label="Họ và tên" value={orderInformation.fullName} onChange={nameChangeHandler} size="small" fullWidth margin="normal" />
                <TextField label="Số điện thoại" value={orderInformation.phone} onChange={phoneChangeHandler} size="small" fullWidth margin="normal" />
                <TextField label="Email" value={orderInformation.email} onChange={emailChangeHandler} size="small" fullWidth margin="normal" />
                <BillingAddressSelect />
                <TextField label="Lời nhắn" value={orderInformation.message} onChange={messageChangeHandler} size="small" multiline minRows={2} fullWidth margin="normal" />
                <Grid container spacing={2} alignItems="center">
                    <Grid item xs={8}>
                        <TextField label="Mã giảm giá" value={orderInformation.voucher} onChange={voucherChangeHandler} size="small" fullWidth margin="normal" />
                    </Grid>
                    <Grid item xs={4}>
                        <Button sx={{ mt: 1 }} color="inherit" variant="outlined" fullWidth>Áp Dụng</Button>
                    </Grid>
                </Grid>
                <FormControl>
                    <FormLabel sx={{ mt: 1 }}>Phương thức thanh toán:</FormLabel>
                    <RadioGroup row>
                        <FormControlLabel sx={{ mr: 3 }} value="banking" control={<Radio />} label="Chuyển khoản" />
                        <FormControlLabel sx={{ mr: 3 }} value="cod" control={<Radio />} label="C.O.D" />
                        <FormControlLabel sx={{ mr: 3 }} value="creditCard" control={<Radio />} label="Thẻ tín dụng" />
                    </RadioGroup>
                </FormControl>
            </Paper>
        </div>
    )
}

export default BillingForm;