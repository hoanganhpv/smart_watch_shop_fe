import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { useEffect, useState } from "react";
import ConfirmPaymentButton from "./ConfirmPaymentButton";

const OrderDetail = () => {
    const [cartProduct, setCartProduct] = useState([]);
    const [cartTotalPrice, setCartTotalPrice] = useState(0);
    const [cartTotalProduct, setCartTotalProduct] = useState(0);

    useEffect(() => {
        // lấy dữ liệu từ local storage
        const localStorageCart = JSON.parse(localStorage.getItem("cart")) || [];

        // thêm phương thức tính tổng tiền cho mỗi sản phẩm trong giỏ hàng theo giá bán và số lượng
        localStorageCart.map(element => {
            return element.totalPrice = () => {
                return element.promotionPrice * element.quantity;
            }
        });

        // tính tổng tiền của đơn hàng
        let totalPrice = 0;
        localStorageCart.forEach(element => {
            totalPrice += element.totalPrice();
        });

        // tính tổng sản phẩm của đơn hàng
        let totalProduct = 0;
        localStorageCart.forEach(element => {
            totalProduct += element.quantity;
        });

        // lưu data trên vào state để xử lý react
        setCartProduct(localStorageCart);
        setCartTotalPrice(totalPrice);
        setCartTotalProduct(totalProduct);
    }, []);

    return (
        <div>
            <h1 style={{ textAlign: "center" }} className="title-text">Đơn hàng</h1>
            <Paper sx={{ mt: 2, p: 1 }} elevation={2}>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Sản Phẩm</b></TableCell>
                                <TableCell align="center"><b>Số Lượng</b></TableCell>
                                <TableCell align="right"><b>Thành Tiền</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {cartProduct.map((element, index) => (
                                <TableRow key={index}>
                                    <TableCell>{index + 1}. {element.name}</TableCell>
                                    <TableCell align="center">{element.quantity}</TableCell>
                                    <TableCell align="right">{element.totalPrice().toLocaleString("en-us")}đ</TableCell>
                                </TableRow>
                            ))}
                            <TableRow sx={{ borderTop: "2px solid grey" }}>
                                <TableCell sx={{ fontWeight: 500 }}>Tổng Cộng</TableCell>
                                <TableCell align="center"><b>{cartTotalProduct}</b></TableCell>
                                <TableCell sx={{ fontWeight: 500 }} align="right">{cartTotalPrice.toLocaleString("en-us")}đ</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell sx={{ fontWeight: 500, color: "red" }}>Giảm Giá</TableCell>
                                <TableCell></TableCell>
                                <TableCell sx={{ fontWeight: 500, color: "red" }} align="right">{0}đ</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell sx={{ fontWeight: 600 }}>Số Tiền Cần Thanh Toán</TableCell>
                                <TableCell></TableCell>
                                <TableCell sx={{ fontWeight: 600 }} align="right">{cartTotalPrice.toLocaleString("en-us")}đ</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
                <ConfirmPaymentButton />
            </Paper>
        </div>
    )
}

export default OrderDetail;