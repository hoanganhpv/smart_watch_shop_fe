import { useEffect } from "react";
import { Container, Grid } from "@mui/material";
import apple from "../../../assets/images/1.png";
import samsung from "../../../assets/images/2.png";
import garmin from "../../../assets/images/3.png";
import { useDispatch, useSelector } from "react-redux";
import { getAllProductsAction } from "../../../actions/product.action";
import ProductCard4 from "../../products/ProductCard4";
import SeeMoreButton from "./SeeMoreButton";

const LastestProduct = () => {
    const dispatch = useDispatch();
    const { allProducts } = useSelector(reduxData => reduxData.productReducer);

    useEffect(() => {
        dispatch(getAllProductsAction())
    }, [dispatch]);

    const appleProduct = allProducts.filter(element => element.name.includes("Apple")).slice(0, 4);
    const samsungProduct = allProducts.filter(element => element.name.includes("Samsung")).slice(0, 4);
    const garminProduct = allProducts.filter(element => element.name.includes("Garmin")).slice(0, 4);

    return (
        <Container sx={{my:2}}>
            <Grid container>
                <Grid item xs={12} textAlign="center">
                    <h1 className="title-text">Sản phẩm nổi bật</h1>
                    <p className="sub-title-text">
                        Đồng hồ thông minh - Công nghệ sang xịn
                    </p>
                </Grid>
            </Grid>

            {/* Apple Products */}
            <Grid container mt={2}>
                <Grid item xs={12}>
                    <img src={apple} style={{width: "100%", borderRadius: 5}} alt="apple" />
                </Grid>
            </Grid>
            <Grid container spacing={2} my={3} className="lastest-product">
                {appleProduct.map((element, index) => <ProductCard4 data={element} key={index} /> )}
            </Grid>
            <SeeMoreButton />

            {/* Samsung Products */}
            <Grid container mt={2}>
                <Grid item xs={12}>
                    <img src={samsung} style={{width: "100%", borderRadius: 5}} alt="samsung" />
                </Grid>
            </Grid>
            <Grid container spacing={2} my={3} className="lastest-product">
                {samsungProduct.map((element, index) => <ProductCard4 data={element} key={index} /> )}
            </Grid>
            <SeeMoreButton />

            {/* Garmin Products */}
            <Grid container mt={2}>
                <Grid item xs={12}>
                    <img src={garmin} style={{width: "100%", borderRadius: 5}} alt="garmin" />
                </Grid>
            </Grid>
            <Grid container spacing={2} my={3} className="lastest-product">
                {garminProduct.map((element, index) => <ProductCard4 data={element} key={index} /> )}
            </Grid>
            <SeeMoreButton />
        </Container>
    )
}

export default LastestProduct;