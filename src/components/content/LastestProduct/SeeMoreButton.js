import { Button, Grid } from "@mui/material";
import { useNavigate } from "react-router-dom";


const SeeMoreButton = () => {
    const navigate = useNavigate();
    const seeMoreHandler = () => {
        navigate("/products");
    }

    return (
        <Grid container>
            <Grid item xs={12} textAlign="center">
                <Button sx={{ width: 150, borderRadius: "2em", letterSpacing: "1px" }} color="inherit" variant="outlined" onClick={seeMoreHandler}>XEM THÊM</Button>
            </Grid>
        </Grid>
    )
}

export default SeeMoreButton;