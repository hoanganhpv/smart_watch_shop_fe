import {
    ADDRESS_INPUT_CHANGE,
    ADD_PRODUCT_TO_ADMIN_CART,
    ADD_PRODUCT_TO_ADMIN_CART_EMPTY_NAME,
    ADMIN_CHECK_ORDER_INFORMATION,
    ADMIN_CREATE_ORDER_ADDRESS_CHANGE,
    ADMIN_CREATE_ORDER_CITY_CHANGE,
    ADMIN_CREATE_ORDER_DISTRICT_CHANGE,
    ADMIN_CREATE_ORDER_EMAIL_CHANGE,
    ADMIN_CREATE_ORDER_ERROR,
    ADMIN_CREATE_ORDER_MESSAGE_CHANGE,
    ADMIN_CREATE_ORDER_NAME_CHANGE,
    ADMIN_CREATE_ORDER_PHONE_CHANGE,
    ADMIN_CREATE_ORDER_RESET_FORM,
    ADMIN_CREATE_ORDER_START,
    ADMIN_CREATE_ORDER_SUCCESS,
    ADMIN_CREATE_ORDER_USERNAME_CHANGE,
    ADMIN_CREATE_ORDER_WARD_CHANGE,
    ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_ERROR,
    ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_START,
    ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_SUCCESS,
    ADMIN_UPDATE_CUSTOMER_ADDRESS_CHANGE,
    ADMIN_UPDATE_CUSTOMER_CITY_CHANGE,
    ADMIN_UPDATE_CUSTOMER_DISTRICT_CHANGE,
    ADMIN_UPDATE_CUSTOMER_ERROR,
    ADMIN_UPDATE_CUSTOMER_FULLNAME_CHANGE,
    ADMIN_UPDATE_CUSTOMER_PHONE_CHANGE,
    ADMIN_UPDATE_CUSTOMER_START,
    ADMIN_UPDATE_CUSTOMER_SUCCESS,
    ADMIN_UPDATE_CUSTOMER_WARD_CHANGE,
    CHECK_CUSTOMER_UPDATE,
    CHECK_ORDER_INFORMATION,
    CHECK_REGISTER_DATA,
    CITY_INPUT_CHANGE,
    CLOSE_ERROR_ALERT,
    CLOSE_LOGIN_REQUIRE_MODAL,
    CLOSE_SUCCESS_ALERT,
    CREATE_NEW_CHECKOUT,
    CREATE_ORDER_API_ERROR,
    CREATE_ORDER_API_START,
    CREATE_ORDER_API_SUCCESS,
    CREATE_ORDER_CLIENT_ERROR,
    CREATE_ORDER_MODAL_CLOSE,
    CREATE_ORDER_MODAL_OPEN,
    CREATE_USER_API_ERROR,
    CREATE_USER_API_START,
    CREATE_USER_API_SUCCESS,
    CREATE_USER_SERVER_ERROR,
    CUSTOMER_ADMIN_PAGINATION_CHANGE,
    CUSTOMER_DELETE_MODAL_CLOSE,
    CUSTOMER_DELETE_MODAL_OPEN,
    CUSTOMER_DETAIL_MODAL_CLOSE,
    CUSTOMER_DETAIL_MODAL_OPEN,
    CUSTOMER_ORDER_MODAL_CLOSE,
    CUSTOMER_ORDER_MODAL_OPEN,
    DECREASE_PRODUCT_QUANTITY,
    DELETE_ORDER_MODAL_CLOSE,
    DELETE_ORDER_MODAL_OPEN,
    DISTRICT_INPUT_CHANGE,
    EMAIL_INPUT_CHANGE,
    GET_ALL_CUSTOMERS_ERROR,
    GET_ALL_CUSTOMERS_START,
    GET_ALL_CUSTOMERS_SUCCESS,
    GET_ALL_ORDERS_ERROR,
    GET_ALL_ORDERS_START,
    GET_ALL_ORDERS_SUCCESS,
    GET_CUSTOMER_DETAIL_ERROR,
    GET_CUSTOMER_DETAIL_START,
    GET_CUSTOMER_DETAIL_SUCCESS,
    GET_ORDER_DETAIL_ERROR,
    GET_ORDER_DETAIL_START,
    GET_ORDER_DETAIL_SUCCESS,
    GET_USER_ADDRESS,
    GET_USER_INFORMATION,
    GET_USER_INFOR_BY_EMAIL_ERROR,
    GET_USER_INFOR_BY_EMAIL_START,
    GET_USER_INFOR_BY_EMAIL_SUCCESS,
    GET_USER_INFOR_BY_PHONE_ERROR,
    GET_USER_INFOR_BY_PHONE_START,
    GET_USER_INFOR_BY_PHONE_SUCCESS,
    GET_USER_INFOR_BY_USERNAME_ERROR,
    GET_USER_INFOR_BY_USERNAME_START,
    GET_USER_INFOR_BY_USERNAME_SUCCESS,
    GET_USER_INFOR_EMAIL_EMPTY,
    GET_USER_INFOR_PHONE_EMPTY,
    GET_USER_INFOR_USERNAME_EMPTY,
    INCREASE_PRODUCT_QUANTITY,
    LOGIN_ERROR,
    MESSAGE_INPUT_CHANGE,
    NAME_INPUT_CHANGE,
    OPEN_LOGIN_REQUIRE_MODAL,
    ORDER_ADMIN_PAGINATION_CHANGE,
    ORDER_DELETE_ERROR,
    ORDER_DELETE_START,
    ORDER_DELETE_SUCCESS,
    ORDER_DETAIL_MODAL_CLOSE,
    ORDER_DETAIL_MODAL_OPEN,
    ORDER_NOTE_CHANGE,
    ORDER_STATUS_CHANGE,
    ORDER_STATUS_UPDATE_ERROR,
    ORDER_STATUS_UPDATE_START,
    ORDER_STATUS_UPDATE_SUCCESS,
    ORDER_UPDATE_ERROR,
    ORDER_UPDATE_START,
    ORDER_UPDATE_SUCCESS,
    PAYMENT_METHOD_INPUT_CHANGE,
    PHONE_INPUT_CHANGE,
    REFRESH_TOKEN_SUCCESS,
    REGISTER_ADDRESS_CHANGE,
    REGISTER_CITY_CHANGE,
    REGISTER_DISTRICT_CHANGE,
    REGISTER_EMAIL_CHANGE,
    REGISTER_FULLNAME_CHANGE,
    REGISTER_PASSWORD_CHANGE,
    REGISTER_PHONE_CHANGE,
    REGISTER_REPEAT_PASSWORD_CHANGE,
    REGISTER_USERNAME_CHANGE,
    REGISTER_WARD_CHANGE,
    USER_LOGIN_SUCCESS,
    USER_LOGOUT_SUCCESS,
    USER_REGISTER_FALSE,
    USER_REGISTER_TRUE,
    VOUCHER_INPUT_CHANGE,
    WARD_INPUT_CHANGE
} from "../constants/user.const";
const addressData = require("../vietnamAddress.json");

const initialState = {
    userData: JSON.parse(sessionStorage.getItem("user")) || null,
    orderAddressList: {
        cityList: addressData,
        districtList: "",
        wardList: ""
    },
    orderInformation: {
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        district: "",
        ward: "",
        note: "",
        voucher: "",
        paymentMethod: ""
    },
    loading: false,
    orderValid: false,
    orderCode: null,
    loginRequireModalOpen: false,
    userRegister: false,
    registerAddressList: {
        cityList: addressData,
        districtList: "",
        wardList: ""
    },
    registerData: {
        username: "",
        password: "",
        repeatPassword: "",
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        district: "",
        ward: "",
    },
    registerValid: false,
    userSuccessAlert: {
        show: false,
        message: ""
    },
    userErrorAlert: {
        show: false,
        message: ""
    },
    renderPage: true,

    // ADMIN STATES
    // 1. order management
    allOrders: [],
    orderDetailModalOpen: false,
    deleteModalOpen: false,
    createOrderModalOpen: false,
    deleteOrderId: "",
    orderDetail: null,
    currentOrderStatus: "",
    orderPagination: {
        totalPages: 0,
        currentPage: 1,
        limit: 10
    },
    adminOrderAddressList: {
        cityList: addressData,
        districtList: "",
        wardList: ""
    },
    adminCreateOrderInfor: {
        customerId: "",
        username: null,
        fullName: "",
        phone: null,
        email: null,
        address: "",
        city: "",
        district: "",
        ward: "",
        note: "",
        voucher: "",
        paymentMethod: ""
    },
    adminCartProduct: [],
    adminOrderValid: false,

    // 2. Customer management
    allCustomers: [],
    customerPagination: {
        totalPages: 0,
        currentPage: 1,
        limit: 10
    },
    customerDetailModalOpen: false,
    customerDeleteModalOpen: false,
    customerId: "",
    customerOrders: [],
    customerDetail: null,
    customerDetailAddressList: {
        cityList: addressData,
        districtList: "",
        wardList: ""
    },
    updateCustomerDataValid: false,
    customerOrderModalOpen: false,
    customerOrderDetail: [],
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGIN_SUCCESS:
            state.userData = action.payload;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Đăng nhập tài khoản thành công";
            break;

        case USER_LOGOUT_SUCCESS:
            state.userData = null;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Đăng xuất tài khoản thành công";
            break;

        case REGISTER_USERNAME_CHANGE:
            state.registerData.username = action.payload;
            break;

        case REGISTER_PASSWORD_CHANGE:
            state.registerData.password = action.payload;
            break;

        case REGISTER_REPEAT_PASSWORD_CHANGE:
            state.registerData.repeatPassword = action.payload;
            break;

        case REGISTER_FULLNAME_CHANGE:
            state.registerData.fullName = action.payload;
            break;

        case REGISTER_EMAIL_CHANGE:
            state.registerData.email = action.payload;
            break;

        case REGISTER_PHONE_CHANGE:
            state.registerData.phone = action.payload;
            break;

        case REGISTER_ADDRESS_CHANGE:
            state.registerData.address = action.payload;
            break;

        case REGISTER_CITY_CHANGE:
            state.registerData.city = action.payload.city;
            state.registerAddressList.districtList = action.payload.districtList;
            break;

        case REGISTER_DISTRICT_CHANGE:
            state.registerData.district = action.payload.district;
            state.registerAddressList.wardList = action.payload.wardList;
            break;

        case REGISTER_WARD_CHANGE:
            state.registerData.ward = action.payload;
            break;

        case CHECK_REGISTER_DATA:
            if (state.registerData.username.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Tên đăng nhập";
                break;
            }
            else if (state.registerData.password.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Mật khẩu";
                break;
            }
            else if (state.registerData.repeatPassword.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Mật khẩu xác nhận";
                break;
            }
            else if (state.registerData.password !== state.registerData.repeatPassword) {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Mật khẩu xác nhận không trùng khớp";
                break;
            }
            else if (state.registerData.fullName.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Họ và tên";
                break;
            }
            else if (state.registerData.email.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Email";
                break;
            }
            else if (state.registerData.phone.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Số điện thoại";
                break;
            }
            else if (state.registerData.address.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Địa chỉ";
                break;
            }
            else if (state.registerData.city.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Tỉnh/Thành phố";
                break;
            }
            else if (state.registerData.district.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Quận/Huyện";
                break;
            }
            else if (state.registerData.ward.trim() === "") {
                state.registerValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Phường/Xã";
                break;
            }
            else {
                state.registerValid = true;
                break;
            }

        case CREATE_USER_API_START:
            state.loading = true;
            break;

        case CREATE_USER_API_SUCCESS:
            state.loading = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Tạo tài khoản mới thành công. Bạn có thể đăng nhập ngay";
            state.userRegister = false;
            // clear form register
            state.registerData.username = "";
            state.registerData.password = "";
            state.registerData.repeatPassword = "";
            state.registerData.fullName = "";
            state.registerData.phone = "";
            state.registerData.email = "";
            state.registerData.address = "";
            state.registerData.city = "";
            state.registerData.district = "";
            state.registerData.ward = "";
            break;

        case CREATE_USER_API_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Lỗi xảy ra trong quá trình tạo tài khoản. Vui lòng thử lại sau";
            break;

        case CREATE_USER_SERVER_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = action.payload;
            break;

        case GET_USER_INFORMATION:
            state.orderInformation.fullName = action.payload.fullName;
            state.orderInformation.phone = action.payload.phone;
            state.orderInformation.email = action.payload.email;
            break;

        case GET_USER_ADDRESS:
            state.orderInformation.address = action.payload.address;
            state.orderInformation.city = action.payload.city;
            state.orderAddressList.districtList = action.payload.districtList;
            state.orderInformation.district = action.payload.district;
            state.orderAddressList.wardList = action.payload.wardList;
            state.orderInformation.ward = action.payload.ward;
            break;

        case NAME_INPUT_CHANGE:
            state.orderInformation.fullName = action.payload;
            break;

        case PHONE_INPUT_CHANGE:
            state.orderInformation.phone = action.payload;
            break;

        case EMAIL_INPUT_CHANGE:
            state.orderInformation.email = action.payload;
            break;

        case ADDRESS_INPUT_CHANGE:
            state.orderInformation.address = action.payload;
            break;

        case CITY_INPUT_CHANGE:
            state.orderInformation.city = action.payload.city;
            state.orderAddressList.districtList = action.payload.districtList;
            break;

        case DISTRICT_INPUT_CHANGE:
            state.orderInformation.district = action.payload.district;
            state.orderAddressList.wardList = action.payload.wardList;
            break;

        case WARD_INPUT_CHANGE:
            state.orderInformation.ward = action.payload;
            break;

        case MESSAGE_INPUT_CHANGE:
            state.orderInformation.note = action.payload;
            break;

        case VOUCHER_INPUT_CHANGE:
            state.orderInformation.voucher = action.payload;
            break;

        case PAYMENT_METHOD_INPUT_CHANGE:
            state.orderInformation.paymentMethod = action.payload;
            break;

        case CHECK_ORDER_INFORMATION:
            if (state.orderInformation.fullName.trim() === "") {
                state.orderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Họ và tên";
                break;
            }
            else if (state.orderInformation.phone.trim() === "") {
                state.orderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Số điện thoại";
                break;
            }
            else if (state.orderInformation.email.trim() === "") {
                state.orderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Email";
                break;
            }
            else if (state.orderInformation.address.trim() === "") {
                state.orderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Địa chỉ";
                break;
            }
            else if (state.orderInformation.city === "") {
                state.orderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Tỉnh/Thành phố";
                break;
            }
            else if (!state.orderInformation.district) {
                state.orderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Quận/Huyện";
                break;
            }
            else if (!state.orderInformation.ward) {
                state.orderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Phường/Xã";
                break;
            }
            else {
                state.orderValid = true;
                break;
            }

        case CREATE_ORDER_API_START:
            state.loading = true;
            break;

        case CREATE_ORDER_API_SUCCESS:
            state.loading = false;
            state.orderCode = action.payload;
            break;

        case CREATE_ORDER_API_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Lỗi xảy ra trong quá trình tạo đơn hàng";
            break;

        case CREATE_ORDER_CLIENT_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = action.payload;
            break;

        case CREATE_NEW_CHECKOUT:
            state.orderValid = false;
            state.orderCode = "";
            break;

        case REFRESH_TOKEN_SUCCESS:
            state.userData = action.payload;
            break;

        case OPEN_LOGIN_REQUIRE_MODAL:
            state.loginRequireModalOpen = true;
            break;

        case CLOSE_LOGIN_REQUIRE_MODAL:
            state.loginRequireModalOpen = false;
            break;

        case USER_REGISTER_TRUE:
            state.userRegister = true;
            break;

        case USER_REGISTER_FALSE:
            state.userRegister = false;
            break;

        case LOGIN_ERROR:
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = action.payload;
            break;

        case CLOSE_ERROR_ALERT:
            state.userErrorAlert.show = false;
            break;

        case CLOSE_SUCCESS_ALERT:
            state.userSuccessAlert.show = false;
            break;

        // ADMIN PAGE REDUCER
        // 1. Order Reducer
        case GET_ALL_ORDERS_START:
            state.loading = true;
            break;

        case GET_ALL_ORDERS_SUCCESS:
            state.loading = false;
            state.allOrders = action.payload.data;
            state.orderPagination.totalPages = action.payload.totalPages;
            break;

        case GET_ALL_ORDERS_ERROR:
            state.loading = false;
            break;

        case ORDER_ADMIN_PAGINATION_CHANGE:
            state.orderPagination.currentPage = action.payload;
            break;

        case CREATE_ORDER_MODAL_OPEN:
            state.createOrderModalOpen = true;
            break;

        case CREATE_ORDER_MODAL_CLOSE:
            state.createOrderModalOpen = false;
            break;

        case ORDER_DETAIL_MODAL_OPEN:
            state.orderDetailModalOpen = true;
            break;

        case ORDER_DETAIL_MODAL_CLOSE:
            state.orderDetailModalOpen = false;
            break;

        case DELETE_ORDER_MODAL_OPEN:
            state.deleteModalOpen = true;
            state.deleteOrderId = action.payload;
            break;

        case DELETE_ORDER_MODAL_CLOSE:
            state.deleteModalOpen = false;
            break;

        case GET_ORDER_DETAIL_START:
            state.loading = true;
            break;

        case GET_ORDER_DETAIL_SUCCESS:
            state.loading = false;
            state.orderDetail = action.payload;
            state.currentOrderStatus = action.payload.status;
            break;

        case GET_ORDER_DETAIL_ERROR:
            state.loading = false;
            break;

        case ORDER_STATUS_CHANGE:
            state.orderDetail.status = action.payload;
            break;

        case ORDER_NOTE_CHANGE:
            state.orderDetail.note = action.payload;
            break;

        case ORDER_STATUS_UPDATE_START:
            state.loading = true;
            break;

        case ORDER_STATUS_UPDATE_SUCCESS:
            state.loading = false;
            state.currentOrderStatus = action.payload;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Update trạng thái đơn hàng thành công";
            state.renderPage = !state.renderPage;
            break;

        case ORDER_STATUS_UPDATE_ERROR:
            state.loading = false;
            break;

        case ORDER_UPDATE_START:
            state.loading = true;
            break;

        case ORDER_UPDATE_SUCCESS:
            state.loading = false;
            state.orderDetailModalOpen = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Update thông tin đơn hàng thành công";
            state.renderPage = !state.renderPage;
            break;

        case ORDER_UPDATE_ERROR:
            state.loading = false;
            break;

        case ORDER_DELETE_START:
            state.loading = true;
            break;

        case ORDER_DELETE_SUCCESS:
            state.loading = false;
            state.deleteModalOpen = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Xóa đơn hàng thành công";
            state.renderPage = !state.renderPage;
            break;

        case ORDER_DELETE_ERROR:
            state.loading = false;
            break;

        // action for case create order input change:
        case ADMIN_CREATE_ORDER_USERNAME_CHANGE:
            state.adminCreateOrderInfor.username = action.payload;
            break;
        case ADMIN_CREATE_ORDER_NAME_CHANGE:
            state.adminCreateOrderInfor.fullName = action.payload;
            break;
        case ADMIN_CREATE_ORDER_EMAIL_CHANGE:
            state.adminCreateOrderInfor.email = action.payload;
            break;
        case ADMIN_CREATE_ORDER_PHONE_CHANGE:
            state.adminCreateOrderInfor.phone = action.payload;
            break;
        case ADMIN_CREATE_ORDER_ADDRESS_CHANGE:
            state.adminCreateOrderInfor.address = action.payload;
            break;
        case ADMIN_CREATE_ORDER_CITY_CHANGE:
            state.adminCreateOrderInfor.city = action.payload.city;
            state.adminOrderAddressList.districtList = action.payload.districtList;
            break;
        case ADMIN_CREATE_ORDER_DISTRICT_CHANGE:
            state.adminCreateOrderInfor.district = action.payload.district;
            state.adminOrderAddressList.wardList = action.payload.wardList;
            break;
        case ADMIN_CREATE_ORDER_WARD_CHANGE:
            state.adminCreateOrderInfor.ward = action.payload;
            break;
        case ADMIN_CREATE_ORDER_MESSAGE_CHANGE:
            state.adminCreateOrderInfor.note = action.payload;
            break;

        case ADMIN_CREATE_ORDER_RESET_FORM:
            state.adminCreateOrderInfor.username = null;
            state.adminCreateOrderInfor.email = null;
            state.adminCreateOrderInfor.phone = null;
            state.adminCreateOrderInfor.fullName = "";
            state.adminCreateOrderInfor.address = "";
            state.adminCreateOrderInfor.city = "";
            state.adminOrderAddressList.districtList = "";
            state.adminCreateOrderInfor.district = "";
            state.adminOrderAddressList.wardList = "";
            state.adminCreateOrderInfor.ward = "";
            state.adminCreateOrderInfor.note = "";
            state.adminCartProduct = [];
            break;

        // case handler for get customer infor by username
        case GET_USER_INFOR_BY_USERNAME_START:
            state.loading = true;
            break;
        case GET_USER_INFOR_BY_USERNAME_SUCCESS:
            state.loading = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Lấy dữ liệu khách hàng thành công";
            state.adminCreateOrderInfor.customerId = action.payload.customerId;
            state.adminCreateOrderInfor.email = action.payload.email;
            state.adminCreateOrderInfor.phone = action.payload.phone;
            state.adminCreateOrderInfor.fullName = action.payload.fullName;
            state.adminCreateOrderInfor.address = action.payload.address;
            state.adminCreateOrderInfor.city = action.payload.city;
            state.adminOrderAddressList.districtList = action.payload.districtList;
            state.adminCreateOrderInfor.district = action.payload.district;
            state.adminOrderAddressList.wardList = action.payload.wardList;
            state.adminCreateOrderInfor.ward = action.payload.ward;
            break;
        case GET_USER_INFOR_BY_USERNAME_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Lấy dữ liệu khách hàng thất bại";
            break;
        case GET_USER_INFOR_USERNAME_EMPTY:
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Vui lòng nhập Tên Đăng Nhập";
            break;

        // case handler for get customer infor by email
        case GET_USER_INFOR_BY_EMAIL_START:
            state.loading = true;
            break;
        case GET_USER_INFOR_BY_EMAIL_SUCCESS:
            state.loading = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Lấy dữ liệu khách hàng thành công";
            state.adminCreateOrderInfor.customerId = action.payload.customerId;
            state.adminCreateOrderInfor.username = action.payload.username;
            state.adminCreateOrderInfor.fullName = action.payload.fullName;
            state.adminCreateOrderInfor.phone = action.payload.phone;
            state.adminCreateOrderInfor.address = action.payload.address;
            state.adminCreateOrderInfor.city = action.payload.city;
            state.adminOrderAddressList.districtList = action.payload.districtList;
            state.adminCreateOrderInfor.district = action.payload.district;
            state.adminOrderAddressList.wardList = action.payload.wardList;
            state.adminCreateOrderInfor.ward = action.payload.ward;
            break;
        case GET_USER_INFOR_BY_EMAIL_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Lấy dữ liệu khách hàng thất bại";
            break;
        case GET_USER_INFOR_EMAIL_EMPTY:
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Vui lòng nhập Email";
            break;

        // case handler for get customer infor by email
        case GET_USER_INFOR_BY_PHONE_START:
            state.loading = true;
            break;
        case GET_USER_INFOR_BY_PHONE_SUCCESS:
            state.loading = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Lấy dữ liệu khách hàng thành công";
            state.adminCreateOrderInfor.customerId = action.payload.customerId;
            state.adminCreateOrderInfor.username = action.payload.username;
            state.adminCreateOrderInfor.email = action.payload.email;
            state.adminCreateOrderInfor.fullName = action.payload.fullName;
            state.adminCreateOrderInfor.address = action.payload.address;
            state.adminCreateOrderInfor.city = action.payload.city;
            state.adminOrderAddressList.districtList = action.payload.districtList;
            state.adminCreateOrderInfor.district = action.payload.district;
            state.adminOrderAddressList.wardList = action.payload.wardList;
            state.adminCreateOrderInfor.ward = action.payload.ward;
            break;
        case GET_USER_INFOR_BY_PHONE_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Lấy dữ liệu khách hàng thất bại";
            break;
        case GET_USER_INFOR_PHONE_EMPTY:
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Vui lòng nhập Số Điện Thoại";
            break;

        case ADD_PRODUCT_TO_ADMIN_CART_EMPTY_NAME:
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Vui lòng chọn Sản phẩm";
            break;

        case ADD_PRODUCT_TO_ADMIN_CART:
            state.adminCartProduct = action.payload;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Thêm sản phẩm thành công";
            break;

        case INCREASE_PRODUCT_QUANTITY:
            state.adminCartProduct = action.payload;
            break;

        case DECREASE_PRODUCT_QUANTITY:
            state.adminCartProduct = action.payload;
            break;

        case ADMIN_CHECK_ORDER_INFORMATION:
            if (state.adminCreateOrderInfor.fullName.trim() === "") {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Họ và tên";
                break;
            }
            else if (state.adminCreateOrderInfor.phone === null) {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Số điện thoại";
                break;
            }
            else if (state.adminCreateOrderInfor.email === null) {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Email";
                break;
            }
            else if (state.adminCreateOrderInfor.address.trim() === "") {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Địa chỉ";
                break;
            }
            else if (state.adminCreateOrderInfor.city === "") {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Tỉnh/Thành phố";
                break;
            }
            else if (!state.adminCreateOrderInfor.district) {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Quận/Huyện";
                break;
            }
            else if (!state.adminCreateOrderInfor.ward) {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Phường/Xã";
                break;
            }
            else if (state.adminCartProduct.length === 0) {
                state.adminOrderValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng thêm Sản Phẩm";
                break;
            }
            else {
                state.adminOrderValid = true;
                break;
            }

        case ADMIN_CREATE_ORDER_START:
            state.loading = true;
            break;

        case ADMIN_CREATE_ORDER_SUCCESS:
            state.renderPage = !state.renderPage;
            state.loading = false;
            state.createOrderModalOpen = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Tạo đơn hàng mới thành công";
            break;

        case ADMIN_CREATE_ORDER_ERROR:
            state.loading = false;
            state.userErrorAlert.show = true;
            state.userErrorAlert.message = "Lỗi xảy ra trong quá trình tạo đơn hàng";
            break;

        // 2. Customer Reducer
        // get all customers reducer:
        case GET_ALL_CUSTOMERS_START:
            state.loading = true;
            break;
        case GET_ALL_CUSTOMERS_SUCCESS:
            state.loading = false;
            state.allCustomers = action.payload.allCustomers;
            state.customerPagination.totalPages = action.payload.totalPages;
            break;
        case GET_ALL_CUSTOMERS_ERROR:
            state.loading = false;
            break;

        case CUSTOMER_ADMIN_PAGINATION_CHANGE:
            state.customerPagination.currentPage = action.payload;
            break;

        // customer detail modal reducer
        case CUSTOMER_DETAIL_MODAL_OPEN:
            state.customerDetailModalOpen = true;
            state.customerId = action.payload
            break;
        case CUSTOMER_DETAIL_MODAL_CLOSE:
            state.customerDetailModalOpen = false;
            break;

        // get customer detail reducer
        case GET_CUSTOMER_DETAIL_START:
            state.loading = true;
            break;
        case GET_CUSTOMER_DETAIL_SUCCESS:
            state.loading = false;
            state.customerDetailAddressList = action.payload.customerDetailAddressList;
            state.customerDetail = action.payload.customerDetail;
            break;
        case GET_CUSTOMER_DETAIL_ERROR:
            state.loading = false;
            break;

        // check customer update data reducer
        case CHECK_CUSTOMER_UPDATE:
            if (!state.customerDetail.fullName) {
                state.updateCustomerDataValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Họ và tên";
                break;
            }
            else if (!state.customerDetail.phone) {
                state.updateCustomerDataValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Số điện thoại";
                break;
            }
            else if (!state.customerDetail.email) {
                state.updateCustomerDataValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Email";
                break;
            }
            else if (!state.customerDetail.address) {
                state.updateCustomerDataValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng nhập Địa chỉ";
                break;
            }
            else if (!state.customerDetail.city) {
                state.updateCustomerDataValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Tỉnh/Thành phố";
                break;
            }
            else if (!state.customerDetail.district) {
                state.updateCustomerDataValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Quận/Huyện";
                break;
            }
            else if (!state.customerDetail.ward) {
                state.updateCustomerDataValid = false;
                state.userErrorAlert.show = true;
                state.userErrorAlert.message = "Vui lòng chọn Phường/Xã";
                break;
            }
            else {
                state.updateCustomerDataValid = true;
                break;
            }

        // admin update customer detail reducer
        case ADMIN_UPDATE_CUSTOMER_START:
            state.loading = true;
            break;
        case ADMIN_UPDATE_CUSTOMER_SUCCESS:
            state.loading = false;
            state.userSuccessAlert.show = true;
            state.userSuccessAlert.message = "Cập nhật thông tin khách hàng thành công";
            // close modal
            state.customerDetailModalOpen = false;
            // render page
            state.renderPage = !state.renderPage;
            break;
        case ADMIN_UPDATE_CUSTOMER_ERROR:
            state.loading = false;
            break;

        // admin update customer input change reducer:
        case ADMIN_UPDATE_CUSTOMER_FULLNAME_CHANGE:
            state.customerDetail.fullName = action.payload;
            break;
        case ADMIN_UPDATE_CUSTOMER_PHONE_CHANGE:
            state.customerDetail.phone = action.payload;
            break;
        case ADMIN_UPDATE_CUSTOMER_ADDRESS_CHANGE:
            state.customerDetail.address = action.payload;
            break;
        case ADMIN_UPDATE_CUSTOMER_CITY_CHANGE:
            state.customerDetail.city = action.payload.city;
            state.customerDetailAddressList.districtList = action.payload.districtList;
            break;
        case ADMIN_UPDATE_CUSTOMER_DISTRICT_CHANGE:
            state.customerDetail.district = action.payload.district;
            state.customerDetailAddressList.wardList = action.payload.wardList;
            break;
        case ADMIN_UPDATE_CUSTOMER_WARD_CHANGE:
            state.customerDetail.ward = action.payload;
            break;

        // customer order modal reducer
        case CUSTOMER_ORDER_MODAL_OPEN:
            state.customerOrderModalOpen = true;
            state.customerOrders = action.payload;
            break;
        case CUSTOMER_ORDER_MODAL_CLOSE:
            state.customerOrderModalOpen = false;
            break;

        // customer delete modal reducer
        case CUSTOMER_DELETE_MODAL_OPEN:
            state.customerDeleteModalOpen = true;
            state.customerId = action.payload
            break;
        case CUSTOMER_DELETE_MODAL_CLOSE:
            state.customerDeleteModalOpen = false;
            break;

        // customer order reducer
        case ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_START:
            state.loading = true;
            break;
        case ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_ERROR:
            state.loading = false;
            break;
        case ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_SUCCESS:
            state.loading = false;
            state.customerOrderDetail = action.payload;
            break;

        default:
            break;
    }

    return { ...state };
};

export default userReducer;