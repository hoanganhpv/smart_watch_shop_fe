import {
    ADD_TO_CART_SUCCESS,
    BUTTON_FILTER_CLICK,
    CLOSE_SNACKBAR_ERROR,
    CLOSE_SNACKBAR_SUCCESS,
    FILTER_BRAND_CHECKED,
    FILTER_BRAND_UNCHECKED,
    FILTER_MAX_PRICE_CHANGE,
    FILTER_MIN_PRICE_CHANGE,
    FILTER_NAME_CHANGE,
    GET_ALL_PRODUCTS,
    GET_PRODUCT_DATA_ERROR,
    GET_PRODUCT_DATA_SUCCESS,
    LOADING_DATA_FINISH,
    LOADING_DATA_PENDING,
    PAGINATION_CHANGE,
    PRODUCT_CART_UPDATE,
    REMOVE_FROM_CART_SUCCESS
} from "../constants/product.const";

const initialState = {
    isLoading: false,
    allProducts: [],
    productFilterData: [],
    filterName: "",
    filterMinPrice: "",
    filterMaxPrice: "",
    filterBrand: [],
    currentPage: 1,
    totalPage: 0,
    limit: 9,
    cartNumber: 0,
    snackbarSuccess: {
        show: false,
        message: ""
    },
    snackbarError: {
        show: false,
        message: ""
    },
}

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCT_DATA_SUCCESS:
            state.productFilterData = action.payload.data;
            state.totalPage = action.payload.totalPage;
            state.isLoading = false;
            break;

        case GET_ALL_PRODUCTS:
            state.allProducts = action.payload;
            break;

        case GET_PRODUCT_DATA_ERROR:
            break;

        case FILTER_NAME_CHANGE:
            state.filterName = action.payload;
            break;

        case FILTER_MIN_PRICE_CHANGE:
            state.filterMinPrice = action.payload;
            break;

        case FILTER_MAX_PRICE_CHANGE:
            state.filterMaxPrice = action.payload;
            break;

        case FILTER_BRAND_CHECKED:
            // user chọn thì push value đó vào mảng
            state.filterBrand.push(action.payload);
            break;

        case FILTER_BRAND_UNCHECKED:
            // user bỏ chọn thì filter value đó ra khỏi mảng
            state.filterBrand = state.filterBrand.filter(brand => brand !== action.payload);
            break;

        case PAGINATION_CHANGE:
            state.currentPage = action.payload;
            break;

        case BUTTON_FILTER_CLICK:
            state.currentPage = 1;
            break;

        case PRODUCT_CART_UPDATE:
            state.cartNumber = action.payload;
            break;

        case ADD_TO_CART_SUCCESS:
            state.snackbarSuccess.show = true;
            state.snackbarSuccess.message = "Thêm Sản Phẩm Vào Giỏ Hàng Thành Công";
            break;

        case REMOVE_FROM_CART_SUCCESS:
            state.snackbarSuccess.show = true;
            state.snackbarSuccess.message = "Xóa Sản Phẩm Thành Công";
            break;

        case CLOSE_SNACKBAR_SUCCESS:
            state.snackbarSuccess.show = false;
            break;

        case CLOSE_SNACKBAR_ERROR:
            state.snackbarError.show = false;
            break;

        case LOADING_DATA_PENDING:
            state.isLoading = true;
            break;

        case LOADING_DATA_FINISH:
            state.isLoading = false;
            break;

        default:
            break;
    }

    return { ...state };
}

export default productReducer;