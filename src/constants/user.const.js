export const USER_LOGIN_SUCCESS = "USER LOGIN SUCCESS";
export const USER_LOGOUT_SUCCESS = "USER LOGOUT SUCCESS";
export const USER_AUTH_ERROR = "USER AUTH ERROR";
export const LOADING_DATA = "LOADING DATA";

export const REGISTER_USERNAME_CHANGE = "REGISTER USERNAME CHANGE";
export const REGISTER_PASSWORD_CHANGE = "REGISTER PASSWORD CHANGE";
export const REGISTER_REPEAT_PASSWORD_CHANGE = "REGISTER REPEAT PASSWORD CHANGE";
export const REGISTER_FULLNAME_CHANGE = "REGISTER FULLNAME CHANGE";
export const REGISTER_EMAIL_CHANGE = "REGISTER EMAIL CHANGE";
export const REGISTER_PHONE_CHANGE = "REGISTER PHONE CHANGE";
export const REGISTER_ADDRESS_CHANGE = "REGISTER ADDRESS CHANGE";
export const REGISTER_CITY_CHANGE = "REGISTER CITY CHANGE";
export const REGISTER_DISTRICT_CHANGE = "REGISTER DISTRICT CHANGE";
export const REGISTER_WARD_CHANGE = "REGISTER WARD CHANGE";

export const CHECK_REGISTER_DATA = "CHECK REGISTER DATA";

export const CREATE_USER_API_START = "CREATE USER API START";
export const CREATE_USER_API_SUCCESS = "CREATE USER API SUCCESS";
export const CREATE_USER_API_ERROR = "CREATE USER API ERROR";
export const CREATE_USER_SERVER_ERROR = "CREATE USER SERVER ERROR";

export const GET_USER_INFORMATION = "GET USER INFORMATION";
export const GET_USER_ADDRESS = "GET USER ADDRESS";

export const NAME_INPUT_CHANGE = "NAME INPUT CHANGE";
export const PHONE_INPUT_CHANGE = "PHONE INPUT CHANGE";
export const EMAIL_INPUT_CHANGE = "EMAIL INPUT CHANGE";
export const ADDRESS_INPUT_CHANGE = "ADDRESS INPUT CHANGE";
export const CITY_INPUT_CHANGE = "CITY INPUT CHANGE";
export const DISTRICT_INPUT_CHANGE = "DISTRICT INPUT CHANGE";
export const WARD_INPUT_CHANGE = "WARD INPUT CHANGE";
export const MESSAGE_INPUT_CHANGE = "MESSAGE INPUT CHANGE";
export const VOUCHER_INPUT_CHANGE = "VOUCHER INPUT CHANGE";
export const PAYMENT_METHOD_INPUT_CHANGE = "PAYMENT METHOD INPUT CHANGE";

export const OPEN_LOGIN_REQUIRE_MODAL = "OPEN LOGIN REQUIRE MODAL";
export const CLOSE_LOGIN_REQUIRE_MODAL = "CLOSE LOGIN REQUIRE MODAL";

export const USER_REGISTER_TRUE = "USER REGISTER TRUE";
export const USER_REGISTER_FALSE = "USER REGISTER FALSE";

export const BUTTON_LOGIN_CLICK = "BUTTON LOGIN CLICK";
export const LOGIN_ERROR = "LOGIN ERROR";
export const CLOSE_SUCCESS_ALERT = "CLOSE SUCCESS ALERT";
export const CLOSE_ERROR_ALERT = "CLOSE ERROR ALERT";

export const CHECK_ORDER_INFORMATION = "CHECK ORDER INFORMATION";
export const CREATE_ORDER_API_START = "CREATE ORDER API START";
export const CREATE_ORDER_API_SUCCESS = "CREATE ORDER API SUCCESS";
export const CREATE_ORDER_API_ERROR = "CREATE ORDER API ERROR";
export const CREATE_ORDER_CLIENT_ERROR = "CREATE ORDER CLIENT ERROR";

export const CREATE_NEW_CHECKOUT = "CREATE NEW CHECKOUT";
export const REFRESH_TOKEN_SUCCESS = "REFRESH TOKEN SUCCESS";

// ADMIN CONSTANT
// 1. Order Constant
// admin get all orders constant
export const GET_ALL_ORDERS_START = "GET ALL ORDERS START";
export const GET_ALL_ORDERS_SUCCESS = "GET ALL ORDERS SUCCESS";
export const GET_ALL_ORDERS_ERROR = "GET ALL ORDERS ERROR";

// admin pagination change constant
export const ORDER_ADMIN_PAGINATION_CHANGE = "ORDER ADMIN PAGINATION CHANGE";

// admin get order detail constant
export const GET_ORDER_DETAIL_START = "GET ORDER DETAIL START";
export const GET_ORDER_DETAIL_SUCCESS = "GET ORDER DETAIL SUCCESS";
export const GET_ORDER_DETAIL_ERROR = "GET ORDER DETAIL ERROR";

// admin order modal constant
export const ORDER_DETAIL_MODAL_OPEN = "ORDER DETAIL MODAL OPEN";
export const ORDER_DETAIL_MODAL_CLOSE = "ORDER DETAIL MODAL CLOSE";
export const DELETE_ORDER_MODAL_OPEN = "DELETE ORDER MODAL OPEN";
export const DELETE_ORDER_MODAL_CLOSE = "DELETE ORDER MODAL CLOSE";
export const CREATE_ORDER_MODAL_OPEN = "CREATE ORDER MODAL OPEN";
export const CREATE_ORDER_MODAL_CLOSE = "CREATE ORDER MODAL CLOSE";

// admin call api update order status constant
export const ORDER_STATUS_CHANGE = "ORDER STATUS CHANGE";
export const ORDER_NOTE_CHANGE = "ORDER NOTE CHANGE";
export const ORDER_STATUS_UPDATE_START = "ORDER STATUS UPDATE START";
export const ORDER_STATUS_UPDATE_SUCCESS = "ORDER STATUS UPDATE SUCCESS";
export const ORDER_STATUS_UPDATE_ERROR = "ORDER STATUS UPDATE ERROR";

// admin call api update order constant
export const ORDER_UPDATE_START = "ORDER UPDATE START";
export const ORDER_UPDATE_SUCCESS = "ORDER UPDATE SUCCESS";
export const ORDER_UPDATE_ERROR = "ORDER UPDATE ERROR";

// admin call api delete order constant
export const ORDER_DELETE_START = "ORDER DELETE START";
export const ORDER_DELETE_SUCCESS = "ORDER DELETE SUCCESS";
export const ORDER_DELETE_ERROR = "ORDER DELETE ERROR";

// admin create order input constant
export const ADMIN_CREATE_ORDER_USERNAME_CHANGE = "ADMIN CREATE ORDER USERNAME CHANGE";
export const ADMIN_CREATE_ORDER_NAME_CHANGE = "ADMIN CREATE ORDER NAME CHANGE";
export const ADMIN_CREATE_ORDER_PHONE_CHANGE = "ADMIN CREATE ORDER PHONE CHANGE";
export const ADMIN_CREATE_ORDER_EMAIL_CHANGE = "ADMIN CREATE ORDER EMAIL CHANGE";
export const ADMIN_CREATE_ORDER_ADDRESS_CHANGE = "ADMIN CREATE ORDER ADDRESS CHANGE";
export const ADMIN_CREATE_ORDER_CITY_CHANGE = "ADMIN CREATE ORDER CITY CHANGE";
export const ADMIN_CREATE_ORDER_DISTRICT_CHANGE = "ADMIN CREATE ORDER DISTRICT CHANGE";
export const ADMIN_CREATE_ORDER_WARD_CHANGE = "ADMIN CREATE ORDER WARD CHANGE";
export const ADMIN_CREATE_ORDER_MESSAGE_CHANGE = "ADMIN CREATE ORDER MESSAGE CHANGE";
export const ADMIN_CREATE_ORDER_RESET_FORM = "ADMIN CREATE ORDER RESET FORM";

// admin get customer infor by username action:
export const GET_USER_INFOR_BY_USERNAME_START = "GET USER INFOR BY USERNAME START";
export const GET_USER_INFOR_BY_USERNAME_SUCCESS = "GET USER INFOR BY USERNAME SUCCESS";
export const GET_USER_INFOR_BY_USERNAME_ERROR = "GET USER INFOR BY USERNAME ERROR";
export const GET_USER_INFOR_USERNAME_EMPTY = "GET USER INFOR USERNAME EMPTY";

// admin get customer infor by email action:
export const GET_USER_INFOR_BY_EMAIL_START = "GET USER INFOR BY EMAIL START";
export const GET_USER_INFOR_BY_EMAIL_SUCCESS = "GET USER INFOR BY EMAIL SUCCESS";
export const GET_USER_INFOR_BY_EMAIL_ERROR = "GET USER INFOR BY EMAIL ERROR";
export const GET_USER_INFOR_EMAIL_EMPTY = "GET USER INFOR EMAIL EMPTY";

// admin get customer infor by phone action:
export const GET_USER_INFOR_BY_PHONE_START = "GET USER INFOR BY PHONE START";
export const GET_USER_INFOR_BY_PHONE_SUCCESS = "GET USER INFOR BY PHONE SUCCESS";
export const GET_USER_INFOR_BY_PHONE_ERROR = "GET USER INFOR BY PHONE ERROR";
export const GET_USER_INFOR_PHONE_EMPTY = "GET USER INFOR PHONE EMPTY";

// admin cart product actions:
export const ADD_PRODUCT_TO_ADMIN_CART = "ADD PRODUCT TO ADMIN CART";
export const ADD_PRODUCT_TO_ADMIN_CART_EMPTY_NAME = "ADD PRODUCT TO ADMIN CART EMPTY NAME";
export const INCREASE_PRODUCT_QUANTITY = "INCREASE PRODUCT QUANTITY";
export const DECREASE_PRODUCT_QUANTITY = "DECREASE PRODUCT QUANTITY";

// admin create new order action:
export const ADMIN_CHECK_ORDER_INFORMATION = "ADMIN CHECK ORDER INFORMATION";
export const ADMIN_CREATE_ORDER_START = "ADMIN CREATE ORDER START";
export const ADMIN_CREATE_ORDER_SUCCESS = "ADMIN CREATE ORDER SUCCESS";
export const ADMIN_CREATE_ORDER_ERROR = "ADMIN CREATE ORDER ERROR";

// 2. Customer Constant
// admin get all customers constant
export const GET_ALL_CUSTOMERS_START = "GET ALL CUSTOMERS START";
export const GET_ALL_CUSTOMERS_SUCCESS = "GET ALL CUSTOMERS SUCCESS";
export const GET_ALL_CUSTOMERS_ERROR = "GET ALL CUSTOMERS ERROR";

// admin pagination change constant
export const CUSTOMER_ADMIN_PAGINATION_CHANGE = "CUSTOMER ADMIN PAGINATION CHANGE";

// admin get customer detail constant
export const GET_CUSTOMER_DETAIL_START = "GET CUSTOMER DETAIL START";
export const GET_CUSTOMER_DETAIL_SUCCESS = "GET CUSTOMER DETAIL SUCCESS";
export const GET_CUSTOMER_DETAIL_ERROR = "GET CUSTOMER DETAIL ERROR";

// admin update customer modal constant
export const CUSTOMER_DETAIL_MODAL_OPEN = "CUSTOMER DETAIL MODAL OPEN";
export const CUSTOMER_DETAIL_MODAL_CLOSE = "CUSTOMER DETAIL MODAL CLOSE";

// admin delete customer modal constant
export const CUSTOMER_DELETE_MODAL_OPEN = "CUSTOMER DELETE MODAL OPEN";
export const CUSTOMER_DELETE_MODAL_CLOSE = "CUSTOMER DELETE MODAL CLOSE";

// admin customer orders modal constant
export const CUSTOMER_ORDER_MODAL_OPEN = "CUSTOMER ORDER MODAL OPEN";
export const CUSTOMER_ORDER_MODAL_CLOSE = "CUSTOMER ORDER MODAL CLOSE";

// admin customer detail/edit address change constant
export const CUSTOMER_DETAIL_ADDRESS_CHANGE = "CUSTOMER DETAIL ADDRESS CHANGE";
export const CUSTOMER_DETAIL_CITY_CHANGE = "CUSTOMER DETAIL CITY CHANGE";
export const CUSTOMER_DETAIL_DISTRICT_CHANGE = "CUSTOMER DETAIL DISTRICT CHANGE";
export const CUSTOMER_DETAIL_WARD_CHANGE = "CUSTOMER DETAIL WARD CHANGE";

// admin update customer detail action:
export const CHECK_CUSTOMER_UPDATE = "CHECK UPDATE CUSTOMER UPDATE";
export const ADMIN_UPDATE_CUSTOMER_START = "ADMIN UPDATE CUSTOMER START";
export const ADMIN_UPDATE_CUSTOMER_SUCCESS = "ADMIN UPDATE CUSTOMER SUCCESS";
export const ADMIN_UPDATE_CUSTOMER_ERROR = "ADMIN UPDATE CUSTOMER ERROR";

// admin update customer input change constant
export const ADMIN_UPDATE_CUSTOMER_FULLNAME_CHANGE = "ADMIN UPDATE CUSTOMER FULLNAME CHANGE";
export const ADMIN_UPDATE_CUSTOMER_PHONE_CHANGE = "ADMIN UPDATE CUSTOMER PHONE CHANGE";
export const ADMIN_UPDATE_CUSTOMER_ADDRESS_CHANGE = "ADMIN UPDATE CUSTOMER ADDRESS CHANGE";
export const ADMIN_UPDATE_CUSTOMER_CITY_CHANGE = "ADMIN UPDATE CUSTOMER CITY CHANGE";
export const ADMIN_UPDATE_CUSTOMER_DISTRICT_CHANGE = "ADMIN UPDATE CUSTOMER DISTRICT CHANGE";
export const ADMIN_UPDATE_CUSTOMER_WARD_CHANGE = "ADMIN UPDATE CUSTOMER WARD CHANGE";

// admin customer order detail constant
export const ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_START = "ADMIN GET ORDER DETAIL OF CUSTOMER START";
export const ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_SUCCESS = "ADMIN GET ORDER DETAIL OF CUSTOMER SUCCESS";
export const ADMIN_GET_ORDER_DETAIL_OF_CUSTOMER_ERROR = "ADMIN GET ORDER DETAIL OF CUSTOMER ERROR";