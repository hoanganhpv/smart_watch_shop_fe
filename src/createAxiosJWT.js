import axios from "axios";
import jwtDecode from "jwt-decode";
import { REFRESH_TOKEN_SUCCESS } from "./constants/user.const";

const createAxiosInterceptorJWT = (dispatch) => {
    // Get User Data from sessionStorage
    const userData = JSON.parse(sessionStorage.getItem("user")) || [];
    // config axios jwt
    const axiosJWT = axios.create();
    axiosJWT.interceptors.request.use(
        async (config) => {
            let date = new Date();
            const decodedToken = jwtDecode(userData.accessToken);
            if (decodedToken.exp < date.getTime() / 1000) {
                // gọi api refresh token:
                const axiosConfig = {
                    method: "post",
                    url: `http://localhost:8000/refresh`,
                    withCredentials: true
                };
                const { data } = await axios.request(axiosConfig);

                // change only accessToken to new one:
                const refreshUser = {
                    ...userData,
                    accessToken: data.accessToken
                };
                config.headers["Token"] = "Bearer " + data.accessToken;
                // save refresh user with token to session storage
                sessionStorage.setItem("user", JSON.stringify(refreshUser));
                // dispatch refresh user to state
                await dispatch({
                    type: REFRESH_TOKEN_SUCCESS,
                    payload: refreshUser
                });
            }
            return config;
        },
        (err) => {
            return Promise.reject(err);
        }
    )
    return axiosJWT;
}

export default createAxiosInterceptorJWT;