import LastestProduct from "../components/content/LastestProduct/LastestProduct";
import ProductSlide from "../components/content/ProductSlide/ProductSlide";
import RiskReducer from "../components/content/RiskReducer/RiskReducer";
import FooterComponent from "../components/footer/FooterComponent";
import HeaderComponent from "../components/header/HeaderComponent";

const HomePage = () => {
    return (
        <div>
            <HeaderComponent />
            <ProductSlide />
            <RiskReducer />
            <LastestProduct />
            <FooterComponent />
        </div>
    )
}

export default HomePage;