import { Container, Typography } from "@mui/material";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import FooterComponent from "../components/footer/FooterComponent";
import HeaderComponent from "../components/header/HeaderComponent";

const AboutUsPage = () => {
    const breadcrumbsAddress = [
        { name: "Trang Chủ", url: "/" },
        { name: "Giới Thiệu", url: "/about" },
    ]
    return (
        <div>
            <HeaderComponent />
            <Container sx={{ minHeight: 560, py: 3 }}>
                <BreadCrumb address={breadcrumbsAddress} />
                <Typography textAlign="center" variant="h4">Giới thiệu <b>2T Smart Shop</b></Typography>
                <Typography sx={{ mt: 3 }} variant="h5">Giới thiệu chung</Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>
                    <b>2T Smart Shop</b> là nhà bán lẻ hàng đầu, chuyên cung cấp các sản phẩm công nghệ chính hãng tại thị trường Việt Nam. Năm 1996, <b>2T Smart Shop</b> được thành lập,
                    từng bước trở thành địa chỉ đáng tin cậy của người tiêu dùng Việt. Với khẩu hiệu “Nếu những gì chúng tôi không có, nghĩa là bạn không cần”,
                    chúng tôi đã, đang và sẽ tiếp tục nỗ lực đem đến các sản phẩm công nghệ chính hãng đa dạng, phong phú đi kèm mức giá tốt nhất phục vụ nhu cầu của quý khách hàng.
                </Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>
                    Sau hơn 20 năm phát triển, <b>2T Smart Shop</b> đã trở thành cái tên không còn xa lạ với người tiêu dùng trong nước. Hiện nay chúng tôi đang sở hữu mạng
                    lưới hơn 100 chi nhánh phủ trên khắp cả nước, trong đó bao gồm hai trung tâm bảo hành tại Hà Nội và một trung tâm bảo hành tại thành phố Hồ Chí Minh.
                    Đến với chuỗi cửa hàng của <b>2T Smart Shop</b>, quý khách có thể hoàn toàn yên tâm về uy tín, chất lượng sản phẩm với mức giá rẻ hơn khoảng 15-20% so với
                    giá bán trên thị trường. Song song với đó, chúng tôi cũng luôn nỗ lực phục vụ đem đến trải nghiệm dịch vụ tốt nhất cho khách hàng.
                </Typography>
                <Typography sx={{ mt: 3 }} variant="h5">Những dấu mốc quan trọng</Typography>
                <ul>
                    <li><b>Năm 1996:</b> Thành lập cửa hàng <b>2T Smart Shop</b> đầu tiên tọa lạc trên con phố sầm uất của Thủ đô Hà Nội ở địa chỉ 194 Lê Duẩn.</li>
                    <li><b>Năm 2000:</b> <b>2T Smart Shop</b> chính thức trở thành nhà phân phối chính hãng hợp tác với nhiều nhãn hàng lớn hàng đầu như Samsung, OPPO, Nokia, Huawei,...</li>
                    <li><b>Năm 2006:</b> Với kinh nghiệm 10 năm trong lĩnh vực bán lẻ, <b>2T Smart Shop</b> phát triển mạnh mẽ và liên tục mở thêm nhiều chi nhánh mới.</li>
                    <li><b>Năm 2016:</b> Kỉ niệm 20 năm hoạt động trong lĩnh vực bán lẻ các sản phẩm công nghệ, <b>2T Smart Shop</b> đã khẳng định được chỗ đứng vững chắc trên thị trường cũng như trong tiềm thức người tiêu dùng.</li>
                    <li><b>Năm 2020:</b> <b>2T Smart Shop</b> tự hào trở thành nhà bán lẻ ủy quyền chính thức của Apple tại Việt Nam. Các sản phẩm Apple chính hãng VN/A do Apple Việt Nam phân phối được bán tại hệ thống <b>2T Smart Shop</b> với mức giá tốt nhất thị trường cùng chế độ bảo hành uy tín. </li>
                </ul>
                <Typography sx={{ mt: 3 }} variant="h5">Tôn chỉ hoạt động </Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>
                    <b>2T Smart Shop</b> luôn hoạt động dựa trên tôn chỉ đặt khách hàng là trung tâm, mọi nỗ lực để đạt được mục tiêu cao nhất là làm hài lòng người dùng
                    thông qua các sản phẩm được cung cấp và dịch vụ khách hàng. <b>2T Smart Shop</b> đang từng bước xây dựng dịch vụ khách hàng vượt trội, xứng đáng là đơn
                    vị bán lẻ hàng đầu tại Việt Nam. Sự tin tưởng và ủng hộ nhiệt tình của quý khách hàng tại chuỗi chi nhánh đã phần nào khẳng định hiệu quả hoạt động
                    của đội ngũ nhân viên <b>2T Smart Shop</b>.
                </Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>Đối với quý khách hàng, chúng tôi luôn đặt cái tâm làm gốc, làm việc với tinh thần nghiêm túc, trung thực và có trách nhiệm, để mang tới trải nghiệm dịch vụ tốt nhất.</Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>Đối với đồng nghiệp, chúng tôi đề cao văn hóa học hỏi, đoàn kết, tương trợ lẫn nhau tạo nên môi trường làm việc tôn trọng - công bằng - văn minh cho nhân viên trong công ty.</Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>Đối với các đối tác, <b>2T Smart Shop</b> luôn làm việc dựa trên nguyên tắc tôn trọng, cùng tạo ra giá trị cho cộng đồng và cùng phát triển bền vững.</Typography>
                <Typography sx={{ mt: 3 }} variant="h5">Tầm nhìn và sứ mệnh</Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>
                    Những năm qua, chúng tôi không ngừng cải thiện dịch vụ tại các chi nhánh và hỗ trợ khách hàng qua các kênh online. <b>2T Smart Shop</b> cam kết mang đến những sản phẩm
                    chất lượng và chế độ bảo hành uy tín, sẵn sàng hỗ trợ khách hàng trong thời gian nhanh nhất.
                </Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>
                    Trong tương lai, <b>2T Smart Shop</b> sẽ tiếp tục mở rộng hệ thống chi nhánh, hướng tới mục tiêu có mặt tại 63 tỉnh thành trên toàn quốc. Đồng thời, nâng cao chất lượng
                    dịch vụ, hạn chế những rủi ro, lắng nghe và tiếp thu góp ý của quý khách hàng nhằm đem đến trải nghiệm tốt nhất khi mua sắm tại <b>2T Smart Shop</b>.
                </Typography>
                <Typography sx={{ mt: 1, fontSize: 15 }}>Cuối cùng, <b>2T Smart Shop</b> hy vọng sẽ trở thành nhà tiên phong đưa những sản phẩm công nghệ mới nhất đến tay người dùng sớm nhất, tạo ra cuộc sống hiện đại
                    nơi công nghệ kết nối con người, công nghệ phục vụ con người.
                </Typography>
            </Container>
            <FooterComponent />
        </div>

    )
}

export default AboutUsPage;